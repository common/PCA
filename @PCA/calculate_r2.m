function r2 = calculate_r2( pca, X )
%Calculates R-squared values.
%
% r2 = pca.calculate_r2( X )
%
% Given the data used to construct the PCA, this calculates the R2 values
% for the reduced representation of the data.  If all of the eigenvalues
% are retained, then this should be unity.
%
% NOTE: this should NOT be used for functions of the original variables.
% In that case, you must perform some regression onto the PCs and then
% calculate the R2 value.  This function can NOT be used for that purpose.
%
% See also convergence r2converge

[npts,nvar] = size(X);
assert(npts>nvar);

% ensure that the X given is the same as the one used to construct the PCA.
%data_consistency_check(pca,X);

xapprox = eta2x(pca,x2eta(pca,X));
r2 = zeros(1,nvar);
for i=1:nvar
  r2(i) = 1 - sum((X(:,i)-xapprox(:,i)).^2) / sum((X(:,i)-mean(X(:,i))).^2);
end
