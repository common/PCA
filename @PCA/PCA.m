classdef PCA
  % A class to support Principal Component Analysis
  %
  % type "doc PCA" for more information
  % to get help on constructing PCA objects, type "doc PCA.PCA"
  %
  % To run regression test:
  %   PCA.test;
  %   for more information, type "doc test"
  %
  % AUTHOR: James C. Sutherland (James.Sutherland@utah.edu)
  
  properties (SetAccess=private, GetAccess=public)
    scaling = 'std'; % scaling method
    XCenter;         % centering factors
    XScale;          % scaling factors
    R;               % covariance matrix
    L;               % eigenvalues
    Q;               % eigenvectors
  end
  
  properties (Access=public)
    neta;            % number of retained eigenvalues
  end
  
  properties (Dependent=true, SetAccess=private, GetAccess=public)
    loadings;        % PC loadings
    nvar;            % number of variables
  end
  
  methods (Access=public)
    eta = x2eta( pca, x, flag );
    x   = eta2x( pca, eta );
    
    us = u_scores(pca,X);
    ws = w_scores(pca,X);
    
    r2 = calculate_r2(pca,X);
    r2 = convergence(pca,X,nmax,names);
    [r2,neta] = r2converge( pca, data, names, fname )
    
    f = plot_convergence(pca);
    set_retained_eigenvalues(pca,method,option);
    eig_bar_plot_maker( pca, neig, DataName)

    result = eq(pca1,pca2);
    result = ne(pca1,pca2);
    
    function pca = PCA( varargin )
      % Construct a PCA object
      %
      % Examples:
      %  pca = PCA( X );
      %  pca = PCA( X, neta );
      %  pca = PCA( X, scaling );
      %  pca = PCA( X, neta, scaling );
      %
      % INPUTS:
      %  X       - matrix of data to apply PCA to.  Variables are in columns and
      %            observations are in rows.  Must have more observations than
      %            variables.
      %  neta    - (optional) number of retained eigenvalues - default is all.
      %  scaling - (optional) default is 'AUTO'
      %            'NONE'          no scaling
      %            'AUTO' 'STD'    scale by std
      %            'PARETO'        scale by std^2
      %            'VAST'          scale by std^2/mean
      %            'RANGE'         scale by (max-min)
      %            'LEVEL'         scale by mean
      %            'MAX'           scale by max value
      %
      
      % copy constructor:
      if nargin==1 && isa(varargin{1},'PCA')
        p = varargin{1};
        pca.scaling = p.scaling;
        pca.XCenter = p.XCenter;
        pca.XScale  = p.XScale;
        pca.R       = p.R;
        pca.L       = p.L;
        pca.Q       = p.Q;
        pca.neta    = p.neta;
      end
      
      switch nargin
        
        case 0  % default constructor
          
        case {1 2 3}
          X = varargin{1};
          [npts,nvar] = size(X);
          if( npts<nvar )
            error('Variables should be in columns; observations in rows.\nAlso ensure that you have more than one observation\n');
          end
          
          pca.neta = nvar;   % default number of retained eigenvalues (all)
          
          % resolve additional arguments: scaling and neta parameters
          if( nargin>1 )
            if ischar(varargin{2})
              pca.scaling = upper(varargin{2});
              if nargin==3
                pca.neta = varargin{3};
              end
            else
              pca.neta = varargin{2};
              assert( length(pca.neta) == 1 );
              if nargin==3
                assert( ischar(varargin{3}) );
                pca.scaling = upper(varargin{3});
              end
            end
            
          end
          
          % center and scale the data
          [X,pca.XCenter,pca.XScale] = center_scale(X,pca.scaling);
          
          % calculate the covariance matrix and the eigenvalue
          % decomposition. Calling cov takes up more memory.  Calculate the
          % correlation matrix manually instead
          %    R = cov(xs,1);
          R = X'*X / npts;
          clear X;
          [Q,L] = eig(R);
          
          % sort the eigenvalues and eigenvectors
          [Lsort,isort] = sort(diag(L),'descend');
          Qsort = Q(:,isort);
          
          pca.R = R;     % covariance matrix.
          pca.Q = Qsort; % eigenvectors arranged by sorted eigenvalues
          pca.L = Lsort; % sorted eigenvalues
          
        otherwise
          error('Invalid use of PCA constructor');
          
      end
    end
      
  end % public methods

  
  methods % get methods
    
    function val = get.nvar(pca)
      val = length(pca.L);
    end
    function val = get.loadings(pca)
      val = zeros(pca.nvar,pca.neta);
      for i = 1 : pca.neta;
        for j = 1 : pca.nvar
          val(j, i) = (pca.Q(j,i) * sqrt(pca.L(i)))/sqrt(pca.R(j,j));
        end
      end
    end
    
  end % get methods
  
  
  methods (Static)
    
    result = test( reBless );
    
  end % static methods
  
  
  methods (Access=private)
    ok = data_consistency_check(pca, X, errorsAreFatal);
  end
  
  methods (Access=private, Static )
    [X,xbar,d] = center_scale( X, scaling );
  end
  
  
end
