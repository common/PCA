function write2file( pca, filename )
%Writes the eigenvector matrix, Centering and scaling vectors to .txt
%
% Example:
%  pca = PCA( x );
%  pca.wite2file('pcaData.txt');
%
% INPUTS:
%  pca      - the PCA object
%  filename - destination data file name in text format
%
% NOTE: This function writes only the eigenvector matrix, centering and
% scaling factors and not all of the pca properties
%
% See also 

% AUTHOR: Amir Biglari
% DATE: August, 2013

fid=fopen(filename,'w');
fprintf(fid, '%s\n','Eigenvectors:');

fclose(fid);
dlmwrite(filename, pca.Q, 'delimiter', ',', 'precision', '%6.12f','-append');

fid=fopen(filename,'a');
fprintf(fid, '\n%s\n','Centering Factors:');
dlmwrite(filename, pca.XCenter, 'delimiter', ',', 'precision', '%6.12f','-append');fclose(fid);

fid=fopen(filename,'a');
fprintf(fid, '\n%s\n','Scaling Factors:');
fclose(fid);
dlmwrite(filename, pca.XScale, 'delimiter', ',', 'precision', '%6.12f','-append');

end

