function x = eta2x( pca, eta )
%Calculate the principal components
%
% Example:
%  eta  = pca.eta2x(x);    % calculate the principal components
%  xrec = pca.eta2x(eta);  % calculate reconstructed variables
%
% INPUTS:
%  pca - the PCA object
%  eta - the PCs
%
% OUTPUT:
%  x - the unscaled, uncentered approximation to the X data.
%
% NOTE: this can also be applied to any function of the PCs to obtain the
% corresponding function of the original variables.
%
% See also x2eta u_scores w_scores

% AUTHOR: James C. Sutherland
% DATE: March, 2009

[npts,neta] = size(eta);
assert( neta==pca.neta );

A = pca.Q(:,1:neta);

% form the approximation to x
x = eta * A';
for i=1:length(pca.XCenter)
  x(:,i) = x(:,i) .* pca.XScale(i) + pca.XCenter(i);
end