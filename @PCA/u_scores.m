function uscores = u_scores(pca,X)
%Calculate the u scores (principal components)
%
%Example:
% uscores = pca.u_scores(X)
%
% U-scores = obtained by using the U-vectors, i.e. the eigenvectors of the
% covariance matrix S. The resulting U-scores are uncorrelated and have
% variances equal to the corresponding eigenvalues.
%
% This is entirely equivalent to x2eta.
%
% See also x2eta eta2x w_scores

uscores=x2eta(pca,X);