function pca = set_retained_eigenvalues( pca, method, option )
%Help determine how many eigenvalues to retain
%
%Example
% pca = pca.set_retained_eigenvalues( method )
%
% This function provides a few methods to select the number of eigenvalues
% to be retained in the PCA reduction.
%
% INPUTS:
%  pca    - the PCA object
%  method - (optional) method to use in selecting retained eigenvalues.
%           Default is 'SCREE GRAPH'
%  option - (optional) if not supplied, information will be obtained
%           interactively.  Only used for the 'TOTAL VARIANCE' and
%           'INDIVIDUAL VARIANCE' methods.
%
% OUTPUT:
%  pca - the PCA object with the number of retained eigenvalues set on it.
%
%
% The following methods are available:
%  'TOTAL VARIANCE'      retain the eigenvalues needed to account for a
%                        specific percentage of the total variance (i.e.
%                        80%). The required number of PCs is then the
%                        smallest value of m for which this chosen
%                        percentage is exceeded.
%
%  'INDIVIDUAL VARIANCE' retain the components whose eigenvalues are
%                        greater than the average of the eigenvalues
%                        (Kaiser, 1960) or than 0.7 times he average of the
%                        eigenvalues (Joliffe 1972). For a correlation
%                        matrix this average equals 1.
%
%  'BROKEN STICK'        select the retained PCs according to the
%                        Broken Stick Model.
%
%  'SCREE GRAPH'         use the scree graph, a plot of the eigenvalues
%                        agaist their indexes, and look for a natural break
%                        between the large and small eigenvalues.

neig = length(pca.L);

if( nargin==1 ) method='SCREE GRAPH'; end

switch upper(method)

  
  case 'TOTAL VARIANCE'
    if( nargin==3 )
      frac = option;
    else
      frac = input('Select the fraction of variance to preserve: ');
    end
    neta = 1;
    if frac>1 || frac<0
      error('fraction of variance must be between 0 and 1');
    end
    tot_var = sum(pca.L);
    neig = length(pca.L);
    fracVar = 0;
    while ((fracVar < frac) && (neta<=neig) )
      fracVar = fracVar + pca.L(neta)/tot_var;
      neta = neta+1;
    end
    pca.neta = neta-1;


  case 'INDIVIDUAL VARIANCE'
    if( nargin==3 )
      fac = option;
    else
      fprintf('Choose threshold between 0 and 1\n(1->Kaiser, 0.7->Joliffe)\n');
      fac = input('');
    end
    assert( fac>0 && fac<=1 );

    cutoff = fac*mean(pca.L);
    neta = 1;
    if pca.L > cutoff
      neta = neig;
    else
      while ((pca.L(neta) > cutoff) && (neta<=neig))
        neta = neta+1;
      end
    end
    pca.neta = neta-1;


  case 'BROKEN STICK'
    neta=1;
    stick_stop =1;
    while ((stick_stop == 1) && (neta<=neig))
      broken_stick = 0;
      for j = neta : neig
        broken_stick = broken_stick + 1/j;
      end
      stick_stop = (pca.L(neta) > broken_stick);
      neta = neta+1;
    end
    pca.neta = neta-1;


  case {'SCREE PLOT' 'SCREE GRAPH'}
    plot_convergence(pca);
    pca.neta = input('Select number of retained eigenvalues: ');


  otherwise
    error( strcat('Unsupported method: ',method) );
    
end