function ws = w_scores( pca, X )
%Calculates the w scores
%
% Example:
%  wscores = pca.w_scores( X );
%
% W-scores = The U vectors are scaled by the inverse of the eigenvalues
% square root, i.e. V = L^-0.5 * U. The W-scores are still uncorrelated and
% have variances equal unity.
%
% See also x2eta u_scores

eval = pca.L(1:pca.neta);
ws = pca.x2eta(X) * diag(1./sqrt(eval));