function iseq = eq( a, b )
% compares two PCA objects for equality.
%
% Example:
%  if (pca1 == pca2)
%    disp('hello');
%  end
%
% See also ne

iseq = 0;  % false

scalErr = abs(a.XScale  - b.XScale ) / max(abs(a.XScale ));
centErr = abs(a.XCenter - b.XCenter) / max(abs(a.XCenter));

RErr = abs(a.R - b.R) / max(abs(a.R));
LErr = abs(a.L - b.L) / max(abs(a.L));
QErr = abs(a.Q - b.Q) / max(abs(a.Q));

tol = 10*eps;

% enforce equality on basic quantities
if strcmp(a.scaling,b.scaling) && ...
    (a.neta == b.neta)  && ...
    all( scalErr < tol ) && ...
    all( centErr < tol ) && ...
    all( RErr    < tol ) && ...
    all( QErr    < tol ) && ...
    all( LErr    < tol )
%     all( a.XScale  == b.XScale  ) && ...
%     all( a.XCenter == b.XCenter ) && ...
%     all( a.L       == b.L       ) && ...
%     all(all( a.R == b.R )) && ...
%     all(all( a.Q == b.Q ))
  iseq = 1;  % true
end