function eta = x2eta( pca, x, nocenter )
%Calculate the principal components given the original data.
%
% Example:
%  eta = pca.x2eta( x )
%
% INPUTS:
%  pca      - the PCA object
%  x        - a set of observations of variables x (observations in rows),
%             unscaled, uncentered. These do not need to be the same 
%             observations as were used to construct the PCA object. They
%             could be, e.g. functions of those variables. 
%  nocenter - [OPTIONAL] Defaults to centering.  A nonzero argument here
%             will result in no centering being applied, even though it may
%             be present in the original PCA transformation.  Use this
%             option only if you know what you are doing.  PC source terms
%             are an example of where we want this to be flagged.
%
% OUTPUT:
%  eta - the principal components.
%
% NOTE: if you supply some function of x then you will obtain the resulting
% function of the PCs.
%
% See also eta2x u_scores w_scores

% AUTHOR: James C. Sutherland
% DATE: March, 2009

neta = pca.neta;
[npts,nvar] = size(x);
assert( nvar == length(pca.L) );

if nargin~=3
  nocenter=0;
end
A = pca.Q(:,1:neta);
if nocenter
  for i=1:nvar
    x(:,i) = x(:,i) / pca.XScale(i);
  end
  eta  = x * A;
else
  for i=1:nvar
    x(:,i) = (x(:,i) -pca.XCenter(i)) / pca.XScale(i);
  end
  eta = x*A;
end