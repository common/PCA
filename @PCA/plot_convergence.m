function f=plot_convergence( pca )
%generates pareto plots of the eigenvalues
%
%Example:
% f = pca.plot_convergence()


% AUTHOR: James C. Sutherland
% DATE: March, 2009

f=figure;
pareto(pca.L);
xlabel('Component index')
%ylabel('Variance Explained')
