function result = test( blessFlag )
% Performs regression testing of the PCA class
%
% Examples:
%  result = PCA.test();  % run tests
%  PCA.test(1);  % generate new gold copies of output.
%
% INPUT:
%  blessFlag - if supplied and equal to 1 then new gold copies will be
%              generated.

result = 1;  % change to 0 if any failures occur.

% note: if you change this, it will break the test and you will have to
% re-bless the results.
npts = 100;
x = [ ...
  sin(linspace(0,pi,npts))' ...
  cos(linspace(0,2*pi,npts))' ...
  linspace(0,pi,npts)' ...
  ];

% test various scaling approaches
% we will ensure that the pca objects are identical and that their
% encode/decode operations produce identical results.
scaling = { 'NONE' 'AUTO' 'STD' 'PARETO' 'VAST'  'RANGE' 'LEVEL' 'MAX' };

neta = 2;

% generate each pca and the encode/decode results
p   = {};
eta = {};
xr  = {};
r2  = zeros(length(scaling),3);
for i=1:length(scaling)
  p{i}   = PCA(x,scaling{i},neta);
  eta{i} = p{i}.x2eta(x);
  xr{i}  = p{i}.eta2x(eta{i});
  r2(i,:)  = p{i}.calculate_r2(x);
end

% re-bless if asked to.
if nargin>=1 && blessFlag
  fprintf('Re-blessing results\n');
  save pca_blessed.mat p eta xr r2;
end

% load the blessed results and compare - echoing results to the screen
gold = load('pca_blessed.mat');

fprintf('\n%-8s %-12s%-8s%-10s\n','Scaling','PCA Object','PCs','Recons. X');
fprintf('--------------------------------------\n');
for i=1:length(scaling)

  if p{i} == gold.p{i}
    pf = 'pass';
  else
    pf = 'FAIL';
    result = 0;
  end
  fprintf('%-11s %-9s',scaling{i},pf);
  
  tol = 10*eps;
  
  if all(size(eta{i})==size(gold.eta{i}))
    maxerr = max( abs(eta{i}-gold.eta{i}) ) ./ max(abs(gold.eta{i}));
    if all( maxerr < tol )
      pf = 'pass';
    else
      pf = 'FAIL';
      result = 0;
    end
  else
    pf = 'FAIL';
    result = 0;
  end
  fprintf('%-8s',pf);
  
  if all(size(xr{i})==size(gold.xr{i}))
    maxerr = max( abs(xr{i}-gold.xr{i}) ) ./ max(abs(gold.xr{i}));
     if all( maxerr < tol )
       pf = 'pass';
     else
       pf = 'FAIL';
     end
  else
    pf = 'FAIL';
    result = 0;
  end
  fprintf('%-6s\n',pf);
  
  if max( abs(r2-gold.r2) ) < tol
    pf = 'pass';
  else
    pf = 'FAIL'
  end
  
end

fprintf('--------------------------------------\n');
