function okay = data_consistency_check( pca, X, errorsAreFatal )
% Checks if the supplied data matrix X is consistent with the PCA object
%
% pca.data_consistency_check( X, errorsAreFatal )
%
% INPUTS:
%  pca            - the PCA object
%  X              - the independent variables
%  errorsAreFatal - (OPTIONAL) flag indicating if an error should be raised
%                   if an incompatibility is detected.

[npts,nvar] = size(X);
if( nargin == 2 )
  errorsAreFatal = 1;
end
pca.neta = nvar;
err = X - eta2x( pca,x2eta(pca,X) );
isBad = any( max(abs(err))./max(abs(X)) > 1e-10 ) || ...
        any( min(abs(err))./min(abs(X)) > 1e-10 );
if isBad && errorsAreFatal
  error('it appears that the data is not consistent with the data used to construct the PCA');
end

okay = ~isBad;