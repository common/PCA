function [X,xbar,d] = center_scale( X, scaling )
% Centers and scales data - used in constructing PCA objects
%
% Example:
%  xs = center_scale( X, opts )
%
% INPUTS:
%   X - uncentered, unscaled data
%   scaling - the scaling methodology

xbar = mean(X);
[npts,nvar] = size(X);

% calculate the standard deviation (required for some scalings)
% we do not use the vectorized form for std because it ends up making a
% copy and taking up precious memory...
dev = 0*xbar;
for i=1:nvar
  dev(i) = std(X(:,i),1);
end

% calculate the kurtosis (required for some scalings)
kurt = 0*xbar;
for i=1:nvar
  kurt(i) = sum((X(:,i)-xbar(i)).^4)/npts / (sum((X(:,i)-xbar(i)).^2)/npts)^2;
end

switch upper(scaling)
  case {'NONE' ''}
    d = ones(1,nvar);
  case {'AUTO' 'STD'}
    % loop helps conserve memory for large arrays.  Otherwise we end up
    % with copies inside of the call to std...
    d = dev;
  case 'VAST'
    % loop helps conserve memory for large arrays.  Otherwise we end up
    % with copies inside of the call to std...
    d = dev.^2 ./ (xbar+eps);
  case 'VAST_2'
    d = dev.^2 .* kurt.^2 ./ (xbar+eps);
  case 'VAST_3'
    d = dev.^2 .* kurt.^2 ./ max(X);
  case 'VAST_4'
    d = dev.^2 .* kurt.^2 ./ (max(X)-min(X));
  case 'RANGE'
    d = max(X)-min(X);
  case 'LEVEL'
    d = xbar;
  case 'MAX'
    d = max(X);
  case 'PARETO'
    % loop helps conserve memory for large arrays.  Otherwise we end up
    % with copies inside of the call to std...
    d = zeros(1,nvar);
    for i=1:nvar
      d(i) = sqrt(std(X(:,i),1));
    end
  otherwise
    error('Unsupported scaling option');
end

for i=1:nvar
  X(:,i) = (X(:,i) - xbar(i)) / d(i);
end
