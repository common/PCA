function [r2,neta] = r2converge( pca, data, names, fname )
%Evaluate r2 values as a function of the number of retained eigenvalues.
%
% Examples:
%  [r2,neta] = pca.r2converge( data );
%  [r2,neta] = pca.r2converge( data, names, 'r2.csv' );
%
% INPUTS:
%   pca - the PCA object
%   data - the data to fit.
%   names - [optional] names of the data
%   fname - [optional] file to output r2 information to
%
% OUTPUT:
%   r2 - [neta,nvar]  The r2 values.  Each column is a different variable and
%        each row is for a different number of retained pcs.
%
% See also calculate_r2 convergence

nvar = pca.nvar;

neta = 1:1:nvar;
netapts = length(neta);

[npts,nvar] = size(data);
r2 = zeros(netapts,nvar);

pca.neta = max(neta);
eta = x2eta(pca,data);
for i=1:netapts
  pca.neta = neta(i);
  r2(i,:) = pca.calculate_r2( data );
end

% dump out information
if( nargin>=3 )
  assert( nvar==length(names) );

  if( nargin==4 )
    fid = fopen(fname,'w');
  else
    fid = 1;
  end
  
  fprintf(fid,'neta');
  for i=1:nvar
    fprintf(fid,',%8s',names{i});
  end
  for i=1:netapts
    fprintf(fid,'\n%4i',i);
    fprintf(fid,',%8.4f',r2(i,:));
  end
  fprintf(fid,'\n');
  
  if nargin==4
    fclose(fid);
  end
 
end