function eig_bar_plot_maker( pca, neig, DataName)
%produces a bar plot of the weight of each state variable in the
%eigenvectors
%
% pca.eig_bar_plot_maker( neig )
%
% INPUTS:
%  pca      - The PCA object
%  neig     - Number of eigenvectors that you want to keep in the plot
%  DataName - A cell containing the name of the variables
%
% OUTPUT:
%
% Example:
%  p = PCA( X );                % generate the PCA object using default scaling
%  p.eig_bar_plot_maker( 3, {'v1', 'v2', 'v3'} );   % make a bar plot of 
%                                                     in first 3 eigenvectors
%                                                     the weight of each variable 
%
figure;
bar( pca.Q(:,1:neig) );
set(gca, 'XTickLabel', DataName);
set(gca, 'XTick', 1:size(pca.Q,1) );

legends={};
for i=1:neig
    if (i==1)
        legends(1,i)={'1st EigVec'};
    elseif (i==2)
        legends(1,i)={'2nd EigVec'};
    elseif (i==3)
        legends(1,i)={'3rd EigVec'};
    else
        legends(1,i)={[num2str(i),'th EigVec']};
    end
end
legend(legends);
ylabel('Weights');