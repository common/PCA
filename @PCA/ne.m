function result = ne(a,b)
% tests two PCA objects for inequality.
%
%Example:
% if pca1 ~= pca2
%  disp('not equal');
% end
%
% See also eq

result = ~eq(a,b);