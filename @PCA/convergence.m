function r2 = convergence( pca, X, nmax, names )
%Print r2 values as a function of number of retained eigenvalues.
%
% pca.convergence( X, nmax )
% pca.convergence( X, nmax, names )
%
% INPUTS:
%  pca   - The PCA object
%  X     - the original dataset
%  nmax  - the maximum number of PCs to consider
%  names - the names of the variables
%
% OUTPUT:
%  r2 - [nmax,nvar] matrix containing the R^2 values for each variable as a
%       function of the number of retained eigenvalues.
%
% Example:
%  p = PCA( X );         % generate the PCA object using default scaling
%  p.convergence(X,5);   % plot R2 values retaining 1-5 eigenvalues.
%
% See also r2converge calculate_r2

[npts,nvar] = size(X);
r2 = zeros(nmax,nvar);

data_consistency_check( pca, X );

fprintf('----------------------------------\n%8s','n Eig');
if( nargin==4 )
  assert( nvar == length(names) );
  for i=1:nvar
    fprintf('%8s ',names{i} )
  end
else
  fprintf('%8i ',1:nvar);
end
fprintf('    Mean\n');
for i=1:nmax
  pca.neta = i;
  r2(i,:) = calculate_r2( pca, X );
  fprintf('%8i',i);
  fprintf('%8.4f ',r2(i,:));
  fprintf('%9.4f\n',mean(r2(i,:)));
end
fprintf('----------------------------------\n');
