function xs = remove_outliers( x, scaling )
% xs = remove_outliers( x, scaling )
%
% Given the original data, x, this detects and removes outliers, producing
% a modified data set xs.
%
% INPUTS:
%   x       - the dataset containing outliers. Each row contains an
%             observation each column contains a variable.
%   scaling - the choice of scaling methodology in the PCA.
%
% OUTPUT:
%   xs - the dataset with outliers removed
%

maxiter = 10; % maximum number of iterations
rtol = 1e-3;  % relative change in means of the values

nRemoved = 0; % number of points removed
converged=0;  % flag for convergence
iter = 1;     % iteration counter

while ~converged
  
  pca = PCA( x, scaling );
  pca.set_retained_eigenvalues( 'total variance', 1 );
  
  % get the eigenvalues and only keep the number
  % corresponding to what we selected above.
  neta = pca.neta;
  eval = pca.L;
  eval = eval(1:neta);
  
  eta   = x2eta(pca,x);  % calculate the principal components
  
  % look for the eigenvalues containing more than 50% or less than 20% of
  % the total variance in the data.  We will use those to identify outliers
  cseval = cumsum(eval)/sum(eval);
  ilarge = find( cseval >= 0.5, 1 );
  ismall = find( cseval >= 0.8, 1 );
  
  % select the retained points
  iDiscardLarge = select_discard_points( eta, eval, 1:ilarge );
  iDiscardSmall = select_discard_points( eta, eval, ismall:neta );
  
  iDiscard = union(iDiscardLarge,iDiscardSmall);
  
  % define the new data (without outliers)
  [npts,nvar] = size(x);
  xs = x( setdiff(1:npts,iDiscard), : );
  nRemoved = nRemoved + length(iDiscard);
  [npts,nvar] = size(xs);
  
  % check for convergence
  pcas = PCA( xs, scaling );
  R = pca.R;
  Rs = pcas.R;
  err = max( max( abs((R-Rs)./Rs) ) );
  fprintf('%-5i err: %7.2e, removed %2i points, %4i remain.\n',iter,err,length(iDiscard),npts);
  if err<rtol
    converged=1;
    fprintf('Outlier removal converged in %i iterations\n',iter);
  else
    % set up for next iteration
    x = xs;
  end
  
  iter = iter+1;
  if iter>maxiter
    fprintf('No convergence after %i iterations. %.2e\n',maxiter,err);
    break;
  end
end % while loop

fprintf('Removed a total of %i points, %i remain.\n',nRemoved,npts);

end

%==========================================================================

function ix = select_discard_points( eta, eval, ii )
if isempty(ii)
  ix=[];
  return;
end

% scale the appropriate pcs to determine the Mahalanobis distance - this is
% stored in the eta array for convenience.  Note that we could get this by
% passing "w_scores" rather than "u_scores" - see the PCA class.
for i=ii
  eta(:,i) = eta(:,i).^2 ./ eval(i);
end

dist = sum( eta(:,ii), 2 );    % the Mahalanobis distance
cutoff = quantile(dist,0.9998); % determine the cutoff - 99.8% quantile
ix = find( dist > cutoff );    % select indices to eliminate
end

%==========================================================================

function x = quantile(dist,quant)
% calculates the requested quantile of a given distribution
% I wrote this because I don't have access to Matlab's statistical toolbox
% on all of the machines I run on.
dist = sort(dist);
I = length(dist)*quant;
if abs(I-round(I))<eps
  x = dist(I);
else
  ilo = floor(I);
  ihi = ceil(I);
  x = dist(ihi)*(I-ilo) + dist(ilo)*(ihi-I);
end
end