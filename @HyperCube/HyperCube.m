function HC = HyperCube( varargin )
% hc = HyperCube( x, y, nmax )
%
% Segregates multidimensional data into hypercubes with at most nmax points.
% Cubes with more than nmax points will be bisected in all directions.
% Child bins containing no points are discarded.
%
% Note that this procedure can result in creating cubes containing very few
% points, since 2^ndim child cubes are created in a division.  Supplying
% nmin and dxmin (see below) can help the algorithm avoid this problem, but
% it is still a big challenge, particularly for high-dimensional data.
%
% Example:
%  Given a set of independent variables X with observations in each row,
%  and a set of dependent variables Y with observations in each row,
%
%    hc = HyperCube( x, y, 100, 30 );
%  will create a HyperCube object. We can then refine it by:
%    hcs = refine( hc );
%  Now hcs will have a refined representation of the data.  We can
%  calculate the R2 value as:
%    r2 = calculate_r2( hcs );
%
%--------------------------------------------------------------------------
% INPUT:
%  x    - independent variables.  Array with observations in rows.
%  y    - dependent variables.  Array with observations in rows.
%  nmax - maximum number of points in a bin.  Bins will be recursively
%         subdivided until all bins have less than nmax points in them.
%  nmin - if supplied, this will attempt to produce cubes with at least
%         nmin points.  This can be very useful for high-dimensional cubes,
%         since a strict binary division in multidimensions can result in
%         child cells with very few data points.
%  dxmin- The minimum size of an edge of a hypercube.
%--------------------------------------------------------------------------
%
% AUTHOR: James C. Sutherland
% Copyright 2007, 2009
%
%--------------------------------------------------------------------------

switch nargin
  case 0  % default constructor
    HC.x=[];
    HC.y=[];
    HC.nmax=[];
    HC.nmin=[];
    HC.dxmin=[];
  case 1  % copy constructor
    hc = varargin{1};
    HC.x     = hc.x;
    HC.y     = hc.y;
    HC.nmax  = hc.nmax;
    HC.nmin  = hc.nmin;
    HC.dxmin = hc.dxmin;
    HC.doNotSplit = hc.doNotSplit;
  case 3
    HC.x     = varargin{1};
    HC.y     = varargin{2};
    HC.nmax  = varargin{3};
    HC.nmin  = [];
    HC.dxmin = [];
  case 4
    HC.x     = varargin{1};
    HC.y     = varargin{2};
    HC.nmax  = varargin{3};
    HC.nmin  = varargin{4};
    HC.dxmin = [];
  case 5
    HC.x     = varargin{1};
    HC.y     = varargin{2};
    HC.nmax  = varargin{3};
    HC.nmin  = varargin{4};
    HC.dxmin = varargin{5};
  otherwise
    error('Invalid arguments');
end

[nrx,ncx]=size(HC.x);
[nry,ncy]=size(HC.y);
if( nrx~=nry )
  error('Inconsistent dimensions in "x" and "y"');
end

% set up the base cube
HC.left     = min(HC.x);
HC.right    = max(HC.x);
HC.centroid = 0.5*(HC.left+HC.right);
HC.npts     = nrx;
HC.cmean    = mean(HC.y,1);
HC.ndepvar  = ncy;

if nargin~=1
  HC.doNotSplit = 0;
end
HC = class( HC, 'HyperCube' );
