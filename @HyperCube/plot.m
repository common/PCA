function plot( varargin )
% plot(hc)
%  Given a HyperCube object, this plots the bounds of the cube as well as
%  the points within it.  It only works in 2D.
%
% Author: James C. Sutherland
% Date: January, 2009
% Copyright 2009

switch nargin
  case 1
    hc     = varargin{1};
    handle = gca;
    label  ='';
  case 2
    hc     = varargin{1};
    handle = varargin{2};
    label  ='';
  case 3
    hc     = varargin{1};
    handle = varargin{2};
    label  = varargin{3};
  otherwise
    error('invalid use of HyperCube::plot');
end

[nr,nc] = size(hc.x);
if( nc~=2 )
  if nc==0
    return;
  end
  error('HyperCube::plot only works in 2D');
end

xlo = hc.left(1);
xhi = hc.right(1);
ylo = hc.left(2);
yhi = hc.right(2);
hold on;
if hc.npts < hc.nmin
  color = 'r.';
else
  color = 'b.';
end
plot( hc.x(:,1),hc.x(:,2),color,'MarkerSize',6);
plot( [xlo,xhi],[ylo,ylo],'k-',...
  [xlo,xhi],[yhi,yhi],'k-',...
  [xlo,xlo],[ylo,yhi],'k-',...
  [xhi,xhi],[ylo,yhi],'k-' );
hold off;
if( ~strcmp(label,'' ))
  text(hc.centroid(1),hc.centroid(2),label);
end
