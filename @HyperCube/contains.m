function result = contains(hc,x)
% result = contains(hc,x)
%
% returns a boolean flag indicating if the given point is contained within
% this hypercube.

assert( length(x) == length(hc.left) );
result = all( x>=hc.left & x<=hc.right );
