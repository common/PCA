function children = refine( varargin )
% children = refine( parent )
%
% Refines the hypercube as needed to achieve the desired statistics.
%
% INPUT:
%   parent - parent HyperCube object
%
% OUTPUT:
%   childSEt - HyperCubeSet object.
%
%--------------------------------------------------------------------------
%
% AUTHOR: James C. Sutherland
% Copyright 2007, 2009
%
%--------------------------------------------------------------------------

switch( nargin )
  case 1
    parent = varargin{1};
    depth  = 0;
  case 2
    parent = varargin{1};
    depth  = varargin{2};
  otherwise
    error('invalid use of HyperCube::refine');
end

if( depth>20 )
  fprintf('NOTE: recursion depth: %i\n',depth);
end

% check for minimum size
if ~isempty(parent.dxmin)
  if max(abs(parent.left-parent.right)) < parent.dxmin
    children = HyperCubeSet(parent);
    return;
  end
end

children = HyperCubeSet();

if( parent.npts > parent.nmax && ~parent.doNotSplit )
  c=split(parent);
  j = 1;
  % recursively refine each child
  for i=1:length(c)
    cube = c{i};
    if cube.npts>parent.nmax
      c2 = refine(cube,depth+1);
      for k=1:length(c2)
        children = set( children, j, c2{k} );
        j = j +1;
      end
    else
      children = set( children, j, cube );
      j = j+1;
    end
  end
else
  children = set( children, 1, parent );
end
