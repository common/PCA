function child = assign_coords( parent, ic )
% child = assign_coords( parent, ic )
%
% Assigns coordinates to a child based on the coordinates of the parent.
%
% INPUT:
%  parent - the parent HyperCube
%  child  - the child  HyperCube
%  ic     - the index for the child.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

child = parent;

[npts,ndim] = size(parent.x);

% determine the n-d coordinates of this child w.r.t its parent.
ix = flat2ix( ic, ndim );

% adjust coordinates depending on which child of the hypercube we are in.
% children are indexed 1-based
for i=1:length(ix)
  if( mod(ix(i),2) )
    child.right(i) = parent.centroid(i);
    child.centroid(i) = (parent.centroid(i)+parent.left(i))/2;
  else
    child.left(i) = parent.centroid(i);
    child.centroid(i) = (parent.centroid(i)+parent.right(i))/2;
  end
end

% find parent points that belong to this child.
lo = child.left;
hi = child.right+eps(child.right);
ix = find( all( parent.x >= repmat(lo,npts,1) &...
                parent.x <  repmat(hi,npts,1), 2 ) );

% reset pertinent data.
child.x     = parent.x(ix,:);
child.y     = parent.y(ix,:);
child.npts  = size(child.y,1);
child.cmean = mean(child.y,1);

for ii=1:ndim
  if(    isequal( child.left (ii), child.right   (ii) ) ...
      || isequal( child.left (ii), child.centroid(ii) ) ...
      || isequal( child.right(ii), child.centroid(ii) ) )
    error('problems defining child geometry (degenerated geometry)');
  end
end