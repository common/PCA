function c = merge( c1, c2 )
% merges c1 and c2 to form c

assert( size(c1.x,2) == size(c2.x,2) );

c = c1;
c.x        = [ c1.x; c2.x; ];
c.y        = [ c1.y; c2.y; ];
c.nmax     = max( c1.nmax,  c2.nmax  );
c.nmin     = min( c1.nmin,  c2.nmin  );
c.dxmin    = min( c1.dxmin, c2.dxmin );
c.left     = min( c1.left,  c2.left  );
c.right    = max( c1.right, c2.right );
c.centroid = 0.5*(c.left+c.right);
c.npts     = length(c.y);
c.cmean    = mean(c.y,1);
