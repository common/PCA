function ic=ix2flat( ix, ndim )
% ic=HCix2flat( ix, ndim )
%
% Determines the flat index from the n-dimensional index in a hypercube
% that has exactly two cells in each dimension.
%
% INPUT:
%  ix - the n-d index for the cell.
%  ndim - the number of dimensions.
%
% OUTPUT:
%  ic   - the flat index for the cell.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

if nargin~=2
   error('Invalid number of arguments');
end

if size(ndim)~=[1,1]
   error('Invalid dimensionality.');
end

nn=2*ones(1,ndim);

if( any(ix)>2 || any(ix)<1 )
   error('Invalid index.');
end

switch ndim
   case 1
      ic = ix;
   case 2
      i1 = ix(1);
      i2 = ix(2);
      ic=sub2ind(nn,i1,i2);
   case 3
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      ic=sub2ind(nn,i1,i2,i3);
   case 4
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      ic=sub2ind(nn,i1,i2,i3,i4);
   case 5
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      ic=sub2ind(nn,i1,i2,i3,i4,i5);
   case 6
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6);
   case 7
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7);
   case 8
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      i8 = ix(8);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7,i8);
   case 9
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      i8 = ix(8);
      i9 = ix(9);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7,i8,i9);
   case 10
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      i8 = ix(8);
      i9 = ix(9);
      i10= ix(10);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10);
   case 11
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      i8 = ix(8);
      i9 = ix(9);
      i10= ix(10);
      i11= ix(11);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11);
   case 12
      i1 = ix(1);
      i2 = ix(2);
      i3 = ix(3);
      i4 = ix(4);
      i5 = ix(5);
      i6 = ix(6);
      i7 = ix(7);
      i8 = ix(8);
      i9 = ix(9);
      i10= ix(10);
      i11= ix(11);
      i12= ix(12);
      ic=sub2ind(nn,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12);

   otherwise
      error('Unsupported dimensionality.');
end
end