function ix=flat2ix( ic, ndim )
% ix=HCflat2ix( ic, ndim )
%
% Determines the n-dimensional index from the flat index in a hypercube
% that has exactly two cells in each dimension.
%
% INPUT:
%  ic   - the flat index for the cell.
%  ndim - the number of dimensions.
%
% OUTPUT:
%  ix - the n-d index for the cell.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

if nargin~=2
  error('Invalid number of arguments');
end
if size(ndim)~=[1,1]
  error('Invalid dimensionality.');
end

nn=2*ones(1,ndim);

switch ndim
  case 1
    ix = ic;
  case 2
    [i1,i2]=ind2sub(nn,ic);
    ix = [i1,i2];
  case 3
    [i1,i2,i3]=ind2sub(nn,ic);
    ix = [i1,i2,i3];
  case 4
    [i1,i2,i3,i4]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4];
  case 5
    [i1,i2,i3,i4,i5]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5];
  case 6
    [i1,i2,i3,i4,i5,i6]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6];
  case 7
    [i1,i2,i3,i4,i5,i6,i7]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7];
  case 8
    [i1,i2,i3,i4,i5,i6,i7,i8]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7,i8];
  case 9
    [i1,i2,i3,i4,i5,i6,i8,i7,i9]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7,i8,i9];
  case 10
    [i1,i2,i3,i4,i5,i6,i7,i8,i10]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7,i8,i10];
  case 11
    [i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11];
  case 12
    [i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12]=ind2sub(nn,ic);
    ix = [i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12];
  otherwise
    error('Unsupported dimensionality.');
end

% indices must be bounded by [1,2].
if( any(ix)>2 || any(ix)<1 )
  error('Invalid index.');
end

end