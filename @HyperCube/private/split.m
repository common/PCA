function children = split( parent )
% children = HCSplit( parent );
%
% Splits the parent hypercube into 2^n children, where n is the
% number of independent variables.
%
% INPUTS:
%   parent - the parent HyperCube
%
% OUTPUT:
%   child - a Cell array of hypercube structs.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Modified January, 2009
% Copyright 2007, 2009
%

[npts,ndim] = size(parent.x);

% fprintf('Splitting cell with centroid: ( ')
% for i=1:length(parent.centroid)
%    fprintf('%1.2e, ',parent.centroid(i));
% end
% fprintf(')\n');

if (ndim ~= length(parent.centroid) )
  error('bin coordinate dimension is inconsistent with independent variables.');
end
if (npts~=size(parent.y,1) )
  error('independent and dependent data are inconsistent');
end

% bisect the hypercube:
nchild = 2^ndim;

% partition parent into children
for i=1:nchild
  child{i} = assign_coords(parent,i);
end

strategy='new';

% determine how many points each child has.
ncpts = zeros(nchild,1);
for i=1:nchild
  ncpts(i) = child{i}.npts;
end

assert( sum(ncpts) == parent.npts );

% recombine some children if necessary.
if ( strcmp(strategy,'new') ...
    && ~isempty(parent.nmin) ...
    && parent.nmin<parent.npts/2 )
  
  inonzero = find(ncpts>0);

  while( min(ncpts(inonzero)) < parent.nmin )

    % index for the child with fewest points
    icrb = find( ncpts == min(ncpts(inonzero)) );

    % Occasionally we may find that we have several points that are
    % the minimum.  Only deal with the first one here.  We will get
    % the next one on the next pass through the while loop.
    if( length(icrb)>1 )
      i=icrb(1);
    else
      i=icrb;
    end

    % get the cartesian index for this child.
    myix = flat2ix(i,ndim);

    % get list of neighbors.
    nptsNgbr = zeros(ndim,1);
    ngbrIx   = zeros(ndim,1);
    for j=1:ndim
      ngbr = myix;
      if myix(j)==1
        ngbr(j) = myix(j)+1;
      else
        ngbr(j) = myix(j)-1;
      end
      ingbr = ix2flat(ngbr,ndim);
      nptsNgbr(j) = child{ingbr}.npts;
      ngbrIx(j) = ingbr;
    end

    % Select neighbor for merge.  Do not consider any neighbors
    % with no points.
    nnbgNz = nptsNgbr( find(nptsNgbr>0) );
    if isempty(nnbgNz)
      % no simple merge is possible.  All eligible bins are empty.  To
      % merge, we would require merger of a hyperplane.
      %fprintf('recombination impossible for centriod:\n');
      %disp(child{find(nptsNgbr)});
      break;
    end
    imerge = find( nptsNgbr == min(nnbgNz) );
    if length(imerge)>1
      imerge=imerge(1);
    end
    ixmerge = ngbrIx(imerge);

    % merge with selected neighbor.
    mergedChild = merge( child{i}, child{ixmerge} );
    ncpts(ixmerge) = ncpts(ixmerge) + ncpts(i);
    ncpts(i)       = 0;
    child{i}       = HyperCube();
    child{ixmerge} = mergedChild;
    
    inonzero = find(ncpts>0);
    
  end % while

  ix = find(ncpts>0);
  if length( ix ) ==1
    child{ix}.doNotSplit = 1;
  end
    
end % isempty

nassigned = 0;
for i=1:length(child)
  nassigned = nassigned + child{i}.npts;
end
assert( nassigned == parent.npts );

j=1;
for i=1:length(child)
  if( child{i}.npts > 0 )
    cc{j} = child{i};
    j=j+1;
  end
end
children = HyperCubeSet(cc);

end  % end of function

