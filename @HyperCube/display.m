function display(HC)

[npts,ndim] = size( HC.x );

fprintf('\n-------------');

fprintf('\n %-10s ','Left:');
for i=1:ndim
  fprintf('%11.3e',HC.left(i));
end

fprintf('\n %-10s ','Right:');
for i=1:ndim
  fprintf('%11.3e',HC.right(i));
end

fprintf('\n %-10s','Centroid:');
for i=1:ndim
  fprintf('%11.3e',HC.centroid(i));
end

fprintf('\n mean :');
for i=1:HC.ndepvar
  fprintf(' %.3e',HC.cmean(i));
end

fprintf('\n npts : %i',HC.npts);
fprintf('\n nvar : %i',HC.ndepvar);
fprintf('\n nmax : %i',HC.nmax);
fprintf('\n nmin : %i',HC.nmin);
fprintf('\n dxmin: %i',HC.dxmin);
if( HC.doNotSplit )
  fprintf('\n Do Not Split');
end
fprintf('\n-------------\n\n');