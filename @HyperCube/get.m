function val = get( HC, propname )

switch lower(propname)
  case 'x'
    val = HC.x;
  case 'y'
    val = HC.y;
  case 'nmax'
    val = HC.nmax;
  case 'nmin'
    val = HC.nmin;
  case 'dxmin'
    val = HC.dxmin;
  case 'left'
    val = HC.left;
  case 'right'
    val = HC.right;
  case 'centroid'
    val = HC.centroid;
  case {'npts' 'n'}
    val = HC.npts;
  case {'mean' 'cmean'}
    val = HC.cmean;
  case {'ny' 'nvar' 'ndepvar'}
    val = HC.ndepvar;
  otherwise
    error(strcat('unknown property: ',propname));
end