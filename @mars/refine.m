function M = refine(M,x,y,varargin)
% Iteratively refine a MARS object until the R2 value converges
%
% Examples:
%  M = refine(M,x,y)
%  M = refine(M,x,y,tol)
%  M = refine(M,x,y,tol,maxiter)
%
%
% INPUTS:
%  M  - MARS object to be refined
%  x  - independent variables
%  y  - dependent variables
%
% OPTIONAL INPUTS:
%  atol    - refinement tolerance.  Refinement will occur until the R2
%            value converges to this tolerance (default is 1e-3)
%  maxiter - maximum number of refinement iterations (default is 30)
%
% OUTPUT:
%  M - MARS object after refinement.
%
% See also create_new_basis regress

% AUTHOR: James C. Sutherland
% DATE: December, 2008

atol    = 1e-3;
maxiter = 30;
switch nargin
  case 3
  case {4 5}
    atol = varargin{1};
    if nargin==5
      maxiter=varargin{2};
    end
  otherwise
    error('invalid use of mars::refine');
end
    

converged = false;
ypred = evaluate(M,x);
r2 = calculate_r2(M,x,y,ypred);

fprintf('\n%9s %8s\n','Iteration','R2 Value');
fprintf('-------------------------\n');

A = [];  % passed through to improve efficiency...

iter = 0;
while (~converged && iter<maxiter)
  fprintf('    %-5.0f %8.5f\n',iter,r2);
  [M,r2new,A,ypred] = create_new_basis(M,x,y,A,ypred);
  if( abs(r2-r2new) < atol )
    % in some cases we can get false convergence on the first iteration.
    % Prevent that by considering the relative change in r2.  If we change
    % by more than 5% then assume we are not converged.
    if abs(r2-r2new)/abs(r2new) < 0.05
      converged=true;
    end
  end
  r2 = r2new;
  iter = iter+1;
end

fprintf('    %-5.0f %8.5f\n',iter,r2);
if converged
  fprintf('convergence after %.0f iterations\n',iter);
else
  fprintf('MAX ITERATION COUNT (%.0f) EXCEEDED!\n',maxiter);
end
fprintf('\nSummary:\n');
fprintf('  Number of basis functions: %.0f\n',length(M.basis));
fprintf('  R Squared: %.5f\n',r2);
fprintf('\n');

end
