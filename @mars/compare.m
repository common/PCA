function compare(M,x,y,yname)
% Generates parity plots with R2 values
%
% compare(M,x,y,yname)
%
% See also calculate_r2

% AUTHOR: James C. Sutherland
% Written December, 2007

[npts,nvar] = size(y);
assert( nvar==1 );

ypred = evaluate(M,x);

R2 = calculate_r2(M,x,y,ypred);

ymax = max(y);
ymin = min(y);
ymid = (ymax+ymin)/2;

figure;
plot(y,ypred,'r.',y,y,'k.');

% put the R2 value on the plot.
xloc = 0.5*(ymid+ymin);
yloc = 0.5*(ymax+ymid);
s = sprintf('R^2=%1.4f',R2);
text(xloc,yloc,s);

if( nargin==4 )
  title(yname);
end
