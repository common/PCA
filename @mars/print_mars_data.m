function print_mars_data(mars,fileID)
% prints a mars object.
% INPUT:
%   mars    - The Mars object.
%   fileID  - File ID for an open file for writing.

%Printing the coefficients 
coefs = mars.coef;

fprintf(fileID,'coefs:\n');
for j = 1:length(coefs)
  fprintf(fileID,'%.16f,',coefs(j));
end

fprintf(fileID,'\n\n');


%Printing the basis (dims, senses, xknots, orders) 
basis = mars.basis;

for j = 1:length(basis)  
  
  fprintf(fileID,'basis%i:\n',j);
  if j == 1
    fprintf(fileID,'UnitBasis\n');
  else
    print_basis(basis{j},fileID);
  end
  fprintf(fileID,'\n');
end
