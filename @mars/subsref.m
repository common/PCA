function y = subsref(M,S)
% supports evaluation of a mars object at a vector of points.
%
% Example:
%  x = rand(3,1);
%  y=5*x;
%  M = mars(x,y);
%  x2 = rand(3,1);
%  y2 = M(x2);  % calls subsref
%  plot(x,y,'k.',x2,y2,'ro');
%
% See also evaluate

% AUTHOR: James C. Sutherland
% DATE: December, 2008

switch S.type
  case '()'
    x = S.subs{:};
    y = evaluate( M, x );
  otherwise
    error('invalid use of subsref');
end