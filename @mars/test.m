function result = test( blessFlag )
% performs testing on the mars and MarsGroup classes.
%
%  result = mars_test;  % run tests
%  mars_test(1);        % re-bless the results

if nargin==0
  blessFlag = 0;
end

result = 1;   % change to 0 if any failures occur
t=0;

%% create a sin function and ensure that we can interpolate it using mars
rtol = 1e-2;
npts   = [10 27 100];
nknots = [4 8 20];
order  = [2 3];
sinres = zeros(length(nknots),length(npts),length(order));
sinr2  = sinres;
sinr2tol = 2e-4;
for i=1:length(npts)
  x = linspace(-pi,pi,npts(i))';
  y = sin(x) + 5;
  ymax = max(y);
  for j=1:length(nknots)
    for k=1:length(order)
      tic;  % start the timer
      m = mars(x,y,sinr2tol,order(k),nknots(j));
      relerr = max( abs(y-m.evaluate(x))/ymax );
      sinres(j,i,k) = relerr;
      sinr2 (j,i,k) = calculate_r2(m,x,y);
      t = t+toc;
      if relerr>rtol
        figure;
        plot(x,y,'k.-',x,m.evaluate(x),'ro-');
        title(strcat('nknot=',num2str(nknots(j)),...
          ', order=',num2str(order(k)),...
          ', npts=',num2str(npts(i))));
      end
    end
  end
end


%% multivariate regression
n1 = 30;
n2 = 33;
x1v = linspace(-pi,pi,n1)';
x2v = linspace(  0,pi,n2)';
[x1,x2] = meshgrid(x1v,x2v);
x1 = reshape(x1,n1*n2,1);
x2 = reshape(x2,n1*n2,1);
load mars_test_ivars.mat xr;
y1 = sin(x1)+cos(x2)+xr-5;
y2 = sin(x1).*cos(x2)+xr-5;
%%
mgtol = 5e-4;
tic;
mg1 = MarsGroup( [x1,x2], [y1,y2], mgtol, 1, 8 ,15);
mg2 = MarsGroup( [x1,x2], [y1,y2], mgtol, 2, 4 ,15);
mvres = zeros(n1*n2,4);
yrec1 = mg1([x1,x2]);
yrec2 = mg2([x1,x2]);
mvres(:,1) = abs(y1-yrec1(:,1));
mvres(:,2) = abs(y2-yrec1(:,2));
mvres(:,3) = abs(y1-yrec2(:,1));
mvres(:,4) = abs(y2-yrec2(:,2));

r2_1 = calculate_r2(mg1,[x1,x2],[y1,y2]);
r2_2 = calculate_r2(mg2,[x1,x2],[y1,y2]);

t=t+toc;


%% compare results to blessed results
if blessFlag==0
  
  gold = load('mars_test.mat');
  
  fprintf('\n\n========== RESULTS ==========\n\n');
  
  % look at change in CPU time - issue warning if it increases much.
  tgold = gold.t;
  fprintf('\ntime = %.1f seconds  (gold=%.1f)\n',t,tgold);
  tchange = (t-tgold)/tgold;
  fprintf('change in computational time = %1.1f%%\n',100*tchange);
  if tchange>0.1
    warning('calculation slowed down over gold standard!\n');
  end  
  
  fprintf('1D Interpolation  : ');
  atol = 1e-10;
  err = max(max(max( abs(gold.sinres-sinres) )));
  err_r2 = max(max(max( abs( sinr2-gold.sinr2 ) )));
  if err > atol || err_r2 > sinr2tol
    result = 0;
    fprintf('FAIL!   Max error = %.2e, R2 diff = %.2e\n\n',err,err_r2);
  else
    fprintf('PASS\n\n');
  end
    
  fprintf('2D regression (1) : ')
  err = max(abs(r2_1-gold.r2_1));
  if err < mgtol
    fprintf('PASS\n');
  else
    result = 0;
    fprintf('FAIL : r2 error = %.2e\n',err);  
    y = mg1([x1,x2]);
    figure;
    subplot(2,2,1); hold on;
    surf(x1v,x2v,reshape(y(:,1),n2,n1)); shading interp;
    scatter3(x1,x2,y1,7,y1,'filled'); axis tight;
    subplot(2,2,2); plot(y1,y1,'k.',y1,y(:,1),'r.')
    subplot(2,2,3); hold on;
    surf(x1v,x2v,reshape(y(:,2),n2,n1)); shading interp;
    scatter3(x1,x2,y2,7,y2,'filled'); axis tight;
    subplot(2,2,4); plot(y2,y2,'k.',y2,y(:,2),'r.')
  end
  
  fprintf('2D regression (2) : ')
  err = max(abs(r2_2-gold.r2_2));
  if err < mgtol
    fprintf('PASS\n');
  else
    result = 0;
    fprintf('FAIL : r2 error = %.2e\n',err);
    y = mg2([x1,x2]);
    figure;
    subplot(2,2,1); hold on;
    surf(x1v,x2v,reshape(y(:,1),n2,n1)); shading interp;
    scatter3(x1,x2,y1,7,y1,'filled'); axis tight;
    subplot(2,2,2); plot(y1,y1,'k.',y1,y(:,1),'r.')
    subplot(2,2,3); hold on;
    surf(x1v,x2v,reshape(y(:,2),n2,n1)); shading interp;
    scatter3(x1,x2,y2,7,y2,'filled'); axis tight;
    subplot(2,2,4); plot(y2,y2,'k.',y2,y(:,2),'r.')
  end

  fprintf('\n=============================\n\n');
  
end

if blessFlag==1
  fprintf('Re-blessing results\n');
  save @mars/mars_test.mat sinres sinr2 r2_1 r2_2 mvres t; % mg1 mg2 t;
  %save mars_test.mat sinres sinr2 xr mg1 mg2 t;
end