classdef mars
  % Supports multivariate adaptive regression.
  %
  % type "doc mars" for more detailed information
  %
  %
  % Example 1:
  %  nx=10000; x=linspace(0,3*pi,nx)'; y=sin(x)+0.2*rand(nx,1);
  %  m = mars(x,y,1e-4);
  %  plot(x,y,'k.',x,m(x),'r-');
  %
  % Example 2:
  %  nx=100; x=linspace(0,2*pi,nx)'; y=tan(x);
  %  m = mars(x,y,1e-3,2,100);
  %  ypred = m(x);
  %  plot(x,y,'k.',x,ypred,'r-');
  %
  %
  % To run regression tests:
  %  mars.test
  %
  % See also test mars
  %
  % AUTHOR: James C. Sutherland
  % DATE: December, 2008
  
  
  properties (SetAccess = private, GetAccess = public)
    coef     = 0;     % regressed coefficient array
    knots    = [];    % knot locations
    order    = [];    % basis function maximum order
    interact = 1;     % ???
    basis    = {};    % basis function set
  end
  

  methods (Access=public)  % public methods
    
    function M = mars( varargin )
      % Construct a MARS object.
      % M = mars( x, y, tol )
      %
      % INPUTS:
      %  x - independent variables.  These should be ordered with each
      %      observation in a separate row and each variable in a column.
      %  y - dependent variables.  These should be ordered with each observation
      %      in a separate row and each variable in a column.
      % OPTIONAL INPUTS:
      %  atol    - refinement tolerance
      %  order   - order for basis functions (default is 2)
      %  nknot   - number of (uniform) bins in x space to use (default is 4).
      %            This may be supplied as a vector to specify a different number
      %            of knots in each direction.
      %  maxiter - maximum number of refinement iterations (default is 30)
      %  intord  - interaction order to consider.  This considers nonlinear
      %            interactions between the independent variables for a
      %            multidimensional case.
      %
      % OUTPUT:
      %  M - a MARS object that can be evaluated at an independent variable
      %      location to get a predictor for y.  See the example below.
      
      switch nargin
        
        case 0  % default constructor
          
        case 1  % copy constructor
          mm=varargin{1};
          if( isa(m,'mars') )
            M.coef     = mm.coef;
            M.knots    = mm.knots;
            M.order    = mm.order;
            M.interact = mm.interact;
            M.basis    = mm.basis;
          else
            error('invalid object to mars constructor');
          end
          
        case {2 3 4 5 6 7}  % full constructor
          x = varargin{1};
          y = varargin{2};
          
          [nr,nc] = size(y);
          if( nc>1 )
            error( 'Not set up for multiple dependent variables yet.\nBe sure that the dependent variables are a column vector\n' );
          end
          
          [nr,nc]=size(x);
          if( nc>nr )
            x=x';
          end
          
          nvar = nc;
          
          M.coef = 0;
          
          % set the knots - avoid end points since the basis functions can become
          % null there.  Knots are evenly spaced at dx/2 : dx : L-dx/2
          if nargin>=5
            nknots = varargin{5};
            if length(nknots) == 1
              nknots = nknots * ones(1,nvar);
            end
          else
            nknots = 4*ones(1,nvar);
          end
          xmin = min(x);
          xmax = max(x);
          for ivar=1:nvar
            n = max( 1, nknots(ivar) );
            dx = (xmax(ivar)-xmin(ivar))/(n);
            M.knots{ivar} = xmin(ivar)+dx/2 : dx : xmax(ivar)-dx/2;
          end
          
          % set the order for the basis functions
          if nargin>=4
            M.order = varargin{4};
            if length(M.order)==1
              M.order = M.order*ones(1,nvar);
            end
          else
            M.order = 2*ones(1,nvar);
          end
          
          if nargin==7
            M.interact = varargin{7};
          else
            M.interact = 2;
          end
          
          M.basis{1} = UnitBasis();
          
          M = regress(M,x,y);
          
          % refine the object to fit the supplied data
          atol = 1e-4;
          maxiter = 30;
          if nargin>2
            atol = varargin{3};
          end
          if nargin>=6
            maxiter = varargin{6};
          end
          M = refine(M,x,y,atol,maxiter);
          
        otherwise
          error('invalid construction of a MARS object');
      end
    end
    
    result = eq(m1,m2);
    result = ne(m1,m2);
    
    M = refine(M,x,y,varargin);
    
    r2 = calculate_r2( M, x, y, ypred );
    
    compare(M,x,y,yname);
    
    y = subsref(M,S);
    
    print_mars_data(M,fileID);
    
  end % public methods
  
  % public static methods
  methods (Static=true, Access=public)
    result = test( blessFlag );
  end % public static methods
  
  methods (Access=private)
    [M,varargout] = create_new_basis( M, x, y, A, ypred);
    [M,A] = regress(M,x,y,A,nmod);
    y = evaluate(M,x);
  end % private methods
  
end % classdef mars