function result = ne(M1,M2)
% compare two mars objects for inequality
%
% Example:
%  m1 ~= m2
%
% See also eq
result = ~eq(M1,M2);