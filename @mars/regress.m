function [M,A] = regress(M,x,y,A,nmod)
% perform least squares regression to determine the coefficients for the basis functions
%
% M = regress(M,x,y)
%
% INPUTS:
%  M - the MARS object
%  x - independent variables
%  y - dependent variables
%
%  A - optionally specify the A matrix.  This will improve efficiency in
%      cases where we already know most of the coefficients.
%  nmod - optionally specify how many columns in A are out of date.
%
% OUTPUTS:
%  M - the MARS object with coefficients updated to fit the supplied data.
%  A - the coefficient matrix.
%
% See also refine create_new_basis

% AUTHOR: James C. Sutherland
% DATE: December, 2008

% set up the LHS matrix - entries are the
% basis functions evaluated at the given values of x.

npts = length(x);
ncoef = length(M.coef);

[nr,nc] = size(x);
if (nc>nr) x=x'; end

if( nargin>=4 && ~isempty(A) )
  for i=size(A,2)-nmod+1 : ncoef
    A(:,i)=M.basis{i}(x);
  end
else
  A = zeros( npts, ncoef );
  for j=1:ncoef
    A(:,j) = M.basis{j}(x);
  end
end

if( rcond(A'*A) < 1e-10 )
  M = [];
  return;
end

[nr,nc] = size(y);
if (nc>nr)  y = y'; end;

% solve the normal equations for the coefficients.
M.coef = ((A'*A)\(A'*y))';