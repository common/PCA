function result = eq(M1,M2)
% compares two mars objects for equality
% See also ne
result = ( length(M1.coef) == length(M2.coef) );

if result
  
  % compare coefficients
  result = all(M1.coef == M2.coef);

  if result
    % compare basis functions
    for i=1:length(M1.basis)
      result = (M1.basis{i} == M2.basis{i});
      if ~result return; end
    end
  end
end