function [M,varargout] = create_new_basis( M, x, y, A, ypred)
%Creates new basis functions to maximally reduce the R2 value.
%
% M = create_new_basis( M, x, y )
%
% INPUTS:
%  M - MARS object to refine.
%  x - independent variables [npts,nxvar]
%  y - dependent variables [npts,nyvar]
%
% OUTPUTS:
%  M  - refined MARS object
%  r2 - OPTIONAL R2 value
%
% See also refine regress

% AUTHOR: James C. Sutherland
% DATE: December, 2008


% how many variables?
[npts,nvar] = size(x);
if nvar>npts
  error('it appears that the x variables are shaped wrong');
end

maxOrder = M.order;


% GOAL: minimize sum of squared errors w.r.t. basis function parameters.
errM = sum( (y-ypred).^2 );
err1=errM;
nbf = length(M.basis);
maxInteractOrder = 10;

% loop over existing basis functions and consider refining them
for ibf=1:nbf
  
  % select the variable for this basis function
  parentBasis = M.basis{ibf};
  
  % calculating the interaction order from the parent basis function
  orders = represented_orders(parentBasis);
  interactOrd = sum(orders);
  if ( interactOrd == maxInteractOrder )
    continue;
  end
  
  dims = represented_dims(parentBasis);
  missingDims = setdiff( 1:nvar, dims );
  
  
  % loop over dimensions that have not yet been refined
  for idim=1:length(missingDims)
    dim = missingDims(idim);
    
    % loop over exponents(orders)
    for iord=1:min( maxOrder(dim), (maxInteractOrder-interactOrd))
    
    % loop over knot positions
    nknot = length(M.knots{dim});
    for iknot = 1:nknot
      knot = M.knots{dim};
      %         bnew1 = BasisFunction( maxOrder(dim), knot(iknot),  1, dim, parentBasis );
      %         bnew2 = BasisFunction( maxOrder(dim), knot(iknot), -1, dim, parentBasis );
%       bnew1 = BasisFunction( min( maxOrder(dim),(maxInteractOrder-interactOrd) ), knot(iknot),  1, dim, parentBasis );
%       bnew2 = BasisFunction( min( maxOrder(dim),(maxInteractOrder-interactOrd) ), knot(iknot), -1, dim, parentBasis );
              bnew1 = BasisFunction( iord, knot(iknot),  1, dim, parentBasis );
              bnew2 = BasisFunction( iord, knot(iknot), -1, dim, parentBasis );
      
      if( has_basis_fn(M,bnew1) || has_basis_fn(M,bnew2) )
        continue;
      end
      m2 = M;
      
      % set new basis function in m2.
      c = m2.coef;
      b = m2.basis;
      
      % see if we can add both basis functions
      b{nbf+1} = bnew1;
      b{nbf+2} = bnew2;
      c = [c,0,0];
      m2.basis = b;
      m2.coef  = c;
      [m2,A] = regress( m2, x, y, A, 2 );
      newcols = A(:,[nbf+1:nbf+2]);
      
      % in some cases we cannot add both new basis functions because they
      % do not form a linearly independent basis.  In that case, try
      % adding just one or the other.
      if isempty(m2)
        
        % try the first basis function alone.
        b = {b{1:nbf+1}};
        c = c(1:nbf+1);
        m2 = M;
        m2.basis = b;
        m2.coef  = c;
        [m2,A] = regress( m2, x, y, A(:,1:nbf+1), 0 );
        if isempty(m2)
          err2 = 2*errM;
        else
          [err2,newYpred] = err_calc(m2,x,y);
        end
        
        % try the second basis function alone.
        m3 = M;
        b{nbf+1} = bnew2;
        m3.basis = b;
        m3.coef  = c;
        A(:,nbf+1) = newcols(:,2);
        [m3,A] = regress( m3, x, y, A(:,1:nbf+1), 0 );
        if ~isempty(m3)
          [err3,y3] = err_calc(m3,x,y);
          if err2<err3
            A(:,nbf+1) = newcols(:,1);
          else
            err2     = err3;
            m2       = m3;
            newYpred = y3;
          end
        elseif ~isempty(m2)
          A(:,nbf+1) = newcols(:,1);
        end
      else
        % we succeded with both basis functions.  calculate the error.
        [err2,newYpred] = err_calc(m2,x,y);
      end
      
      % if the candidate basis function reduces the error, keep it.
      % Otherwise, keep looking.
      if err2 < err1
        m1 = m2;
        err1 = err2;
        ypred = newYpred;
      end
      
    end % knot loop
    
    end % exponent(order) loop
    
  end % missing variable dim loop
  
end % basis function loop


if( err1 >= errM )
  warning('create_new_basis did not result in an error reduction!');
else
  if ~isempty(m1)
    M = m1;
  elseif ~isempty(m2)
    M = m2;
  else
    error('error in updating Mars object!');
  end
end

% calculate the R2 value if requested in output.
if( nargout>1 )
  varargout{1} = M.calculate_r2(x,y,ypred);
end
if( nargout>2 )
  varargout{2} = A;
  if ( nargout==4)
    varargout{3} = ypred;
  end
end

end % create_new_basis

%====================================================================

function [err, ypred] = err_calc(M,x,y)
ypred = evaluate(M,x);
err = sum( (y-ypred).^2 );
end

%====================================================================

function res = has_basis_fn( M, b )
res = 0;
% for i=1:length(M.basis)
%   if b==M.basis{i}
mb = M.basis;
for i=1:length(mb)
  if b==mb{i}
    res=1;
    return;
  end
end
end