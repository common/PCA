function r2 = calculate_r2( M, x, y, ypred )
%  Calculates the R2 value for the MARS object given the dependent data, y.
%
% Example:
%  M = mars(x,y);
%  r2 = calculate_r2( M, x, y, ypred );
%  r2 = calculate_r2( M, x, y );
%
% INPUTS:
%  M - the MARS object
%  x - the independent variables
%  y - the dependent variables
%  ypred - [optional] if provided, this method will be faster as the
%          predicted value will not be evaluated internally.
%
% OUTPUT:
%  r2 - the R-squared value
%
% See also evaluate compare

% AUTHOR: James C. Sutherland
% DATE: January, 2009

if nargin==3
  ypred = evaluate(M,x);
end
r2 = 1-sum( (y-ypred).^2, 1 ) ./ sum( (y-mean(y)).^2, 1 );
