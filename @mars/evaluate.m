function y = evaluate(M,x)
% evaluates the mars object at the specified points
%
% See also subsref

mb = M.basis;
mc = M.coef;

y = mc(1) * mb{1}(x);
nb = length(mb);
for i=2:1:nb
  y = y + mc(i)*mb{i}(x);
end

% if( any( abs(y - eval(char(M)) )> max(abs(y))*1e-10 ) )
%   error('inconsistent');
% end