function write2file( M, fid, dvarname)
%Writes all of the mars object properties into a .txt file
%
% Example:
%  m = mars(x,y);
%  fid = fopen('test.txt','w');
%  wite2file(m,fid);
%
% INPUTS:
%  m        - the mars object
%  fid      - destination data file ID in text format that we want to write
%             in it and it is created by fopen
%  dvarname - dependent variable name, by default is 'dvar'
%
% NOTE: 
% 
%
% See also 

% AUTHOR: Amir Biglari
% DATE: August, 2013


if (nargin<3)
    dvarname = 'dvar';
end

fprintf(fid, '%s %s:\n','mars properties for',dvarname);
fprintf(fid, '%s: %d\n','nivar',length(M.knots));
fprintf(fid, '%s:\n','coefs');
fprintf(fid, '%6.16f,',M.coef);
fprintf(fid, '\n');

for ib=1:length(M.basis)
    fprintf(fid,'\n%s%s:\n','basis',num2str(ib));
    write2file(M.basis{ib},fid);
end
fprintf(fid, '\n');
end

