function print_to_file(mg,filename,variablename)
% prints each mars object in the MarsGroup.
%
% Examples:
%  mg = print_to_file(mg,'C:/path/to/file','StateVar');
% INPUT:
%   mg - The MarsGroup object.
%   filename - Name of the text file to be written as well as its path
%   variablename - Name of the dependent variables 
%                  Should usually be either 'StateVar' or 'PCSourceTerm' 

ny = mg.ndvar;

fileID = fopen(filename,'w');


%Printing independent var total
%Redundant but implemented to match Naveen's original input files
numindvars = mg.nivar;
fprintf(fileID,'nivar: %i\n',numindvars);

%Printing var names
fprintf(fileID,'iVarNames: ');
for i = 1:numindvars
    if i ~= numindvars
        fprintf(fileID, 'PC_%i,',i-1);
    else
        fprintf(fileID, 'PC_%i',i-1);
    end
end
fprintf(fileID,'\n');

for iy=1:ny
  fprintf('\nPrinting mars object %i of %i\n',iy,ny);
  
  mars = mg.mobjs{iy};
  
  
  %printing name of the variable
  fprintf(fileID,'mars properties for %s_%i:\n',variablename,(iy-1));
  
  %printing independent var total
  fprintf(fileID,'nivar: %i\n',mg.nivar);

  print_mars_data(mars,fileID) 

end