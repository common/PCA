function compare(MG,x,y,ynames)
% Generate parity plots containing R2 values for each dependent variable.
%
% compare( MG, x, y, ynames )
%
% INPUTS:
%   x - independent variables
%   y - dependent variables
%   ynames (OPTIONAL) - names of dependent variables
%
% See also mars/compare

check(MG,x,y);

for iy=1:MG.ndvar
  if nargin==4
    nam = ynames{iy};
  else
    nam = num2str(iy);
  end
  compare( MG.mobjs{iy}, x, y(:,iy), nam );
end
