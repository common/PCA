classdef MarsGroup
  % Creates a group of mars objects - useful for multiple dependent variables.
  %
  % Example 1:
  %  nx=1000; nx2=900;
  %  x1=2*pi*rand(nx1,1); x2=2*pi*rand(nx2,1);
  %  y1 = sin(x1).*cos(x2);
  %  y2 = sin(x1)+cos(x2);
  %  x = [x1,x2]; y=[y1,y2];
  %
  %  m1 = mars(x,y1,1e-4);
  %  m2 = mars(x,y2,1e-4);
  %  y1pred = m1(x);
  %  y2pred = m2(x);
  %
  %  mm = MarsGroup(x,y,1e-4);
  %  ypred = mm(x);
  %
  %  plot(x,y,'k.',x,m(x),'r-');
  %
  % See also mars refine subsref
  
  properties (SetAccess=private, GetAccess=public)
    ndvar;    % number of dependent variables
    nivar;    % number of independent variables
    mobjs;    % mars objects (cell struct)
  end
  

  methods (Access=public) % public methods

    function MG = MarsGroup( varargin )
      % mm = MarsGroup( x, y, tol, maxorder, ndivisions, maxiter );
      %
      % Multidimensional adaptive regression for a set of dependent variables
      %
      % INPUT:
      %   x - The independent variables [npts,nvar]
      %   y - The dependent   variables [npts,ny]
      %
      % OPTIONAL INPUT:
      %   tol        - tolerance on R2 value for adaption.
      %   maxorder   - Maximum order for polynomials
      %   ndivisions - Number of bins for the data
      %   maxiter    - maximum number of refinement iterations
      %
      %  Currently all optional inputs are scalar valued.
      %
      % See also mars
      %
      
      % AUTHOR: James C. Sutherland
      % DATE: February, 2009
      
      switch nargin
        case 1
          mg = varargin{1};
          assert( isa(mg,'MarsGroup') );
          MG.ndvar = mg.ndvar;
          MG.nivar = mg.nivar;
          MG.mobjs = mg.mobjs;
          
        case {2 3 4 5 6}
          x = varargin{1};
          y = varargin{2};
          [npts,nx]=size(x);
          [n   ,ny]=size(y);
          
          if(n~=npts)
            error('Number of points in X and Y must be the same');
          end
          
          MG.nivar = nx;
          MG.ndvar = ny;
          MG.mobjs = cell(1,ny);
          
          MG = refine(MG,x,y,varargin{3:nargin});
          
        otherwise
          error('improper use of MarsGroup constructor');
      end
    end
    
    r2 = calculate_r2(MG,x,y);
    
    print_to_file(mg,filename,varaiblename);
    
    compare(MG,x,y,ynames);
    
    val = subsref(MG,S);
    
    mg = refine(mg,x,y,varargin);
    
  end % public methods
  
  
  methods (Access=private) % private methods
    check(MG,x,y);
  end % private methods
  
end % classdef MarsGroup