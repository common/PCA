function check(MG,x,y)

[npts,nx] = size(x);
[npts,ny] = size(y);

if( nx ~= MG.nivar )
  error('Inconsistent number of independent variables');
end
if( ny ~= MG.ndvar )
  error('Inconsistent number of dependent variables');
end

