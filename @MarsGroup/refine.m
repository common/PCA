function mg = refine(mg,x,y,varargin)
% refines each mars object in the MarsGroup.
%
% Examples:
%  mg = refine(mg,x,y);
%  mg = refine(mg,x,y,tol,maxord,ndiv,maxiter);
%
% INPUT:
%   mg - The MarsGroup object.
%   x  - The independent variables [npts,nvar]
%   y  - The dependent   variables [npts,ny]
%
% OPTIONAL INPUT:
%   tol        - tolerance on R2 value for adaption.
%   maxorder   - Maximum order for polynomials
%   ndivisions - Number of bins for the data
%   maxiter    - maximum number of refinement iterations
%
% See also mars/refine

check(mg,x,y);

ny = mg.ndvar;

for iy=1:ny
  fprintf('\nRefining for dependent variable %i of %i\n',iy,ny);
  if( isempty(mg.mobjs{iy}) )
    m = mars(x,y(:,iy),varargin{:});
  else
    m = refine( mg.mobjs{iy}, x, y(:,iy), varargin{:} );
  end
  mg.mobjs{iy} = m;
end
