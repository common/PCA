function val = subsref(MG,S)
% Allows simple evaluation of an entire MarsGroup at a set of independent variables.
%
% Examples:
%  y = MG(x); % evaluate all mars objects at the given independent variable.
%  m1 = MG{1};  % obtain the first mars object in this group.
%
% See also mars/subsref

switch S.type
  
  case '()'
    x = S.subs{:};
    [npts,nx] = size(x);
    if nx~=MG.nivar
      error('inconsistent number of indpendent variables');
    end
    val = zeros(npts,MG.ndvar);
    for iy=1:MG.ndvar
      val(:,iy) = MG.mobjs{iy}( x );
    end
    
  case '{}'
    i = S.subs{:};
    val = MG.mobjs{i};
    
  otherwise
    error('invalid use of subsref');
end