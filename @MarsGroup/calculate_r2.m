function r2 = calculate_r2(MG,x,y)
%Calculate the r2 value for all objects in this MarsGroup
%
% r2 = calculate_r2(MG,x,y);
%
% See also mars/calculate_r2

check(MG,x,y);

r2 = zeros(1,MG.ndvar);
for iy=1:MG.ndvar
  r2(iy) = calculate_r2( MG.mobjs{iy}, x, y(:,iy) );
end
