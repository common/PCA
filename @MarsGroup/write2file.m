function write2file( MG, filename, dvarnames)
%Writes all of the mars objects and their properties to a .txt file
%
% Example:
%  mg = MarsGroup(x,y);
%  wite2file(mg,'marsgroupData.txt');
%
% INPUTS:
%  MG        - the MarsGroup object
%  filename  - destination data file name in text format
%  dvarnames - vector of dependent variable names, by default is ['dvar1',
%              'dvar2', ...]
%
% NOTE: 
% 
%
% See also 

% AUTHOR: Amir Biglari
% DATE: August, 2013

ndvar=MG.ndvar;
if (nargin==2)
    dvarnames=cell(1,ndvar);
    for idvar=1:ndvar
        dvarnames{idvar}=['dvar',num2str(idvar)];
    end
end

fid=fopen(filename,'w');
fprintf(fid, '%s: %d\n','nivar',MG.nivar);
for im=1:MG.ndvar
    M=MG.mobjs{im};
    write2file(M,fid,dvarnames{im});
end

end

