function compare( hcs, names )
% compare( hc, names )
%
% compare the conditional mean in the HyperCubeSet with the raw data and
% generate parity plots.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

nvar = hcs.ndepvar;
nc = length(hcs);

R2 = calculate_r2(hcs);

for ivar=1:nvar
  ymin=9e99;
  ymax=-9e99;
  figure;
  hold on;
  for i=1:nc
    hc = hcs.cubes{i};
    nn   = get(hc,'npts');
    if nn==0
      continue;
    end
    mean = get(hc,'cmean');
    y    = get(hc,'y');
    tmp = ones(nn,1) * mean(ivar);
    ydata = y(:,ivar);
    plot(ydata,tmp,'r.',ydata,ydata,'k.');
    ymin = min( ymin, min(ydata) );
    ymax = max( ymax, max(ydata) );
  end
  hold off;
  ymid = (ymax+ymin)/2;
  xloc = 0.5*(ymid+ymin);
  yloc = 0.5*(ymax+ymid);
  s = sprintf('%1.4f',R2(ivar));
  text( xloc, yloc, strcat('R^2=',s) );
  if( nargin==2 )
    title(names{ivar});
  end
end