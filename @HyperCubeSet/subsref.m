function val = subsref(HCS,S)

switch S.type
  case '{}'
    assert( length(S.subs) == 1 );
    ix = S.subs{1};
    val = HCS.cubes{ix};
  case '()'
    assert( length(S.subs)==1 );
    x = S.subs{1};
    [nr,nc] = size(x);
    val = zeros(nr,HCS.ndepvar);
    for irow=1:nr
      found=false;
      % look for the hypercube containing this x.
      for i=1:HCS.ncube
        if contains( HCS.cubes{i}, x(irow,:) )
          found = true;
          val(irow,:) = get(HCS.cubes{i},'mean');
        end
      end
      if ~found
        val(irow,:) = 0;
        warning('No HyperCube containing the requested point was found');
      end
    end
  otherwise
    error('invalid use of HyperCubeSet::subsref')
end
