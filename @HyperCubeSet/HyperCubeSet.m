function HCS = HyperCubeSet( varargin )
%  HCS = HyperCubeSet( {hc1, hc2, ... hcn} );
%    Creates a HyperCubeSet from a cell array of HyperCube objects
%
%  HCS = HyperCubeSet( hc );
%    Creates a HyperCubeSet from a single HyperCube object
%
%  HCS = HyperCubeSet();
%    Creates an empty HyperCubeSet.
%
%  Common methods:
%    calculate_r2 : calculate the R2 value.
%    compare      : generage parity plots with R2 values.
%    plot         : for 2 independent variables, show the partition plot.
%    stats        : look at partioning statistics
%
%--------------------------------------------------------------------------
%
% AUTHOR: James C. Sutherland
% Copyright 2009
%
%--------------------------------------------------------------------------

switch nargin
  case 0
    HCS.ncube   = 0;
    HCS.cubes{1}=HyperCube();
  case 1
    x = varargin{1};
    if( isa(x,'HyperCubeSet') )
      HCS.ncube = x.ncube;
      HCS.cubes = x.cubes;
    elseif( isa(x,'HyperCube') )
      HCS.ncube    = 1;
      HCS.cubes{1} = x;
    elseif( isa(x,'cell') )
      HCS.ncube = length(x);
      HCS.cubes = x;
    else
      error('unrecognized argument to HyperCubeSet');     
    end
  otherwise
    error('invalid use of HyperCubeSet');
end

ntot = 0;
nv1 = get(HCS.cubes{1},'nvar');
for i=1:HCS.ncube
  cube = HCS.cubes{i};
  assert( isvalid(cube) );
  assert( nv1 == get(HCS.cubes{i},'nvar') );
  ntot = ntot + get(cube,'npts');
end
HCS.ntot    = ntot;
HCS.ndepvar = nv1;

HCS = class(HCS,'HyperCubeSet');
