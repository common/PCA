function hcs = set( hcs, varargin )

propertyArgIn = varargin;

while length(propertyArgIn) >= 2
  prop = propertyArgIn{1};
  val = propertyArgIn{2};
  propertyArgIn = propertyArgIn(3:end);

  if prop>hcs.ncube
    hcs.ncube = prop;
  end
  hcs.cubes{prop} = val;
  
  % reset count of points
  n=0;
  for i=1:length(hcs)
    n=n+get(hcs.cubes{i},'n');
  end
  hcs.ntot = n;
  
  if( hcs.ndepvar==0 || length(hcs)==1 )
    hcs.ndepvar = get(val,'nvar');
  end
end