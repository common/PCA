function [xc,yc] = extract_cond_mean( hcs )
% [xc,yc] = extract_cond_mean( hcs )
%
% Extract the conditional mean of the data in the hypercube structure into
% the output arrays
%
% INPUT:
%  hc - The HyperCubeSet object
%
% OUTPUTS:
%  xc - the independent variables (hypercube centroids).  Each row
%       represents a different hypercube.  Each column represents a
%       different dimension (indepent variable).
%  yc - The conditional mean of the dependent variables at the locations
%       defined by xc.  Each column represents a different variable.  Each
%       row represents a different hypercube.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

[npts,nx] = size(get(hcs.cubes{1},'x'));
[n,   ny] = size(get(hcs.cubes{1},'y'));

nc = length(hcs);

xc = zeros(nc,nx);
yc = zeros(nc,ny);

for i=1:nc
  xc(i,:) = get(hcs.cubes{i},'centroid');
  yc(i,:) = get(hcs.cubes{i},'cmean');
end