function plot(hcs)

figure; hold on;
for i=1:length(hcs)
  plot( hcs.cubes{i}, gca, num2str(i) );
end
hold off;