function [maxpts,minpts,stdpts,n] = stats( hcs )
% [maxpts,minpts,stdpts,n] = stats( hcs )
%
% Obtain information about the HyperCubeSet:
%  maxpts - The maximum number of points in all HyperCubes
%  minpts - The minimum number of points in all HyperCubes
%  stdpts - The standard deviation of the number of points
%  n      - A vector containing the number of points in each HyperCube.
nmax = 0;
nmin = 1e9;

n = zeros(hcs.ncube,1);

for i=1:hcs.ncube
  n(i) = get(hcs.cubes{i},'npts');
end

maxpts = max(n);
minpts = min(n);
stdpts = std(n);
