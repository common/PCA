function r2 = calculate_r2( hcs )
% r2 = calculate_r2( hcs )
%
% Calculate the R2 values for the data in n-dimensional space.
%
% INPUT:
%   hcs - a HyperCubeSet
% OUTPUT:
%   r2  - the R-squared value.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

% The R2 value is defined as:
%
%  r2 = 1-\frac{ sum_{i=1}^n (y_i-yp_i)^2 }{ sum_{i=1}^n (y_i-ym)^2 }
%
% where y_i is the observed data, yp_i is the predicted data, and ym is the
% mean of y_i.

nc   = hcs.ncube;
npts = hcs.ntot;

nvar = hcs.ndepvar;
r2 = zeros(1,nvar);

for ivar=1:nvar
  
  tmpsum= 0;
  for i=1:nc
    y = get(hcs.cubes{i},'y');
    tmpsum = tmpsum + sum( y(:,ivar) );
  end
  ymean = tmpsum/npts;

  num=0;
  den=0;
  for i=1:nc
    hc = hcs.cubes{i};
    y = get(hc,'y');
    yi = y(:,ivar);
    if length(yi)>0
      tmp = get(hc,'cmean');
      yp = tmp(ivar);
      num = num + sum( (yi-yp).^2 );
      den = den + sum( (yi-ymean).^2 );
    end
  end

  r2(ivar) = 1 - num/den;
  
end