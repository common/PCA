function [xc,ystd] = extract_std_dev( hcs )
% [xc,ystd] = extract_std_dev( hcs )
%
% Extract the standard deviation of the dependent variables from the mean.
%
% INPUT:
%  hc - The HyperCubeSet object
%
% OUTPUTS:
%  xc - the independent variables (hypercube centroids).  Each row
%       represents a different hypercube.  Each column represents a
%       different dimension (indepent variable).
%  yc - The conditional mean of the dependent variables at the locations
%       defined by xc.  Each column represents a different variable.  Each
%       row represents a different hypercube.
%
% AUTHOR: James C. Sutherland
% Written December, 2007
% Copyright 2007
%

[npts,nx] = size( get(hcs.cubes{1},'x') );
[n,   ny] = size( get(hcs.cubes{1},'y') );

nc = length(hcs);

xc   = zeros(nc,nx);
ystd = zeros(nc,ny);

for i=1:nc
   xc  (i,:) = get(hcs.cubes{i},'centroid');
   ystd(i,:) = std( get(hcs.cubes{i},'y') );
end