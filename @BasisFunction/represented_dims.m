function dim = represented_dims( b )
% interrogate this BasisFunction object to determine how many dimensions
% are represented by it.

if b.haveParent
  dim = [ b.dim, represented_dims(b.parent) ];
else
  dim = b.dim;
end