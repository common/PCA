function y = evaluate(B,x)
% y = evaluate(B,x)
%  evaluate the basis function at the given location.
%
% INPUTS:
%  B - the BasisFunction object
%  x - The independent variables.  These should be arranged with each
%      variable in a column and each observation in a row.

% jcs need to speed this up!!!
[nrx,ncx] = size(x);

ivar = B.dim;
assert( ivar <= ncx );

bkterm = B.sense*(x(:,ivar)-B.xknot);
bkterm(bkterm<=0) = 0;

y = bkterm;
for i=2:1:B.order
  y = y.*bkterm;
end

if( B.haveParent )
  if isa(B.parent,'BasisFunction')
    y = y.*evaluate(B.parent,x);
  end
end
