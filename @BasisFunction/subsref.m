function y = subsref(B,S)
%
% supports evaluation of a BasisFunction at a vector of points.
%
% Example:
%  x = linspace(0,1);
%  b = BasisFunction(2,0.5,1);
%  y = b(x);  % calls subsref...
%

% AUTHOR: James C. Sutherland
% DATE: December, 2008

switch S.type
  case '()'
    x = S.subs{:};
    nb = length(B);
    if nb>1
      error('BasisFunction evaluation is not vectorized');
    end

   y = evaluate(B,x);
    
  otherwise
    error('invalid use of subsref');
end