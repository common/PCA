function display(b)
% displays a BasisFunction object

% AUTHOR: James C. Sutherland
% DATE: December, 2008

fprintf(' xknot: ');
fprintf('%-11.3e',b.xknot);
fprintf('\n order: %i\n', b.order);
fprintf(' sense: %i\n',b.sense);
fprintf(' dim  : %i\n',b.dim);
if( b.haveParent )
  fprintf('Parent:\n')
  display(b.parent);
end
