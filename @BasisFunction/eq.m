function result = eq(B1,B2)

if ~isa(B2,'BasisFunction')
  result = 0;
  return;
end

result = (B1.order == B2.order);

if result
  result = (B1.xknot == B2.xknot);
  if result
    result = (B1.sense == B2.sense);
    if result
      result = B1.haveParent == B2.haveParent;
      if result && B1.haveParent
        result = B1.parent == B2.parent;
      end
    end
  end
end