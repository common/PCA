function print_basis(B,fileID)
% prints the basis info contained in a mars object.
% INPUT:
%   B    - The Basis Function object.
%   fileID  - File ID for an open file for writing.

if isa(B.parent,'UnitBasis')
  fprintf(fileID,'dims: %i,\n',B.dim);
  fprintf(fileID,'senses: %i,\n',B.sense);
  fprintf(fileID,'xknots: %.16f,\n',B.xknot);
  fprintf(fileID,'orders: %i,\n',B.order);
elseif isa(B.parent.parent,'UnitBasis')
  fprintf(fileID,'dims: %i,%i,\n',B.dim,B.parent.dim);
  fprintf(fileID,'senses: %i,%i,\n',B.sense,B.parent.sense);
  fprintf(fileID,'xknots: %.16f,%.16f,\n',B.xknot,B.parent.xknot);
  fprintf(fileID,'orders: %i,%i,\n',B.order,B.parent.order);
elseif isa(B.parent.parent.parent,'UnitBasis')
  fprintf(fileID,'dims: %i,%i,%i,\n',B.dim,B.parent.dim,B.parent.parent.dim);
  fprintf(fileID,'senses: %i,%i,%i,\n',B.sense,B.parent.sense,B.parent.parent.sense);
  fprintf(fileID,'xknots: %.16f,%.16f,%.16f,\n',B.xknot,B.parent.xknot,B.parent.parent.xknot);
  fprintf(fileID,'orders: %i,%i,%i,\n',B.order,B.parent.order,B.parent.parent.order);
else
  warning('More than two parent functions\nThis is currently not supported\n');
end