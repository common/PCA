function [dims,senses,xknots,orders]=write2file( B, fid, write_or_pass, dims, senses, xknots, orders)
%Writes the properties of a BasisFunction and its parents into a .txt file
%
% Example:
%  B = BasisFunction(...);
%  fid = fopen('test.txt','w');
%  wite2file(B,fid);
%
% INPUTS:
%  MG       - the MarsGroup object
%  fid      - destination data file ID in text format that we want to write
%             in it and it is created by fopen
%  write_or_pass - a boolean showing if we want to write the file or we are 
%                  still looking for other parents 
%  dims     - a vector holding all the BasisFunction and its parents dims 
%             respectively
%  senses   - a vector holding all the BasisFunction and its parents senses
%             respectively
%  xknots   - a vector holding all the BasisFunction and its parents xknots
%             respectively
%  orders   - a vector holding all the BasisFunction and its parents orders
%             respectively
%
% NOTE: 
% 
%
% See also 

% AUTHOR: Amir Biglari
% DATE: August, 2013

if (nargin<7)
    if (nargin>3)
        error('You should specify either all of the dims, senses, orders and xknots or nono of them');
    end
    dims=[];
    senses=[];
    orders=[];
    xknots=[];
    if (nargin<3)
        write_or_pass=1;
    end
end
dims=[dims,B.dim];
senses=[senses,B.sense];
xknots=[xknots,B.xknot];
orders=[orders,B.order];
if (B.haveParent)
    p=B.parent;
    if( isa(p,'BasisFunction') )
        [dims,senses,xknots,orders]=write2file( p, fid, false, dims, senses, xknots, orders);
    elseif(isa(p,'UnitBasis'))
        %do nothing
    end
end

if (write_or_pass)
    fprintf(fid, '%s: ','dims');
    fprintf(fid, '%d,',dims);
    fprintf(fid, '\n');

    fprintf(fid, '%s: ','senses');
    fprintf(fid, '%d,',senses);
    fprintf(fid, '\n');
    
    fprintf(fid, '%s: ','xknots');
    fprintf(fid, '%6.16f,',xknots);
    fprintf(fid, '\n');
    
    fprintf(fid, '%s: ','orders');
    fprintf(fid, '%d,',orders);
    fprintf(fid, '\n');
end

end

