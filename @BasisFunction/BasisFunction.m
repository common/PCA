function B = BasisFunction( varargin )
% Constructs a BasisFunction object.
%
% BasisFunction( order, xknot, sense, dim )
% BasisFunction( order, xknot, sense, dim, parent )
% BasisFunction( BasisFunction )
%
% Parameters:
%  order - The order of the basis function.
%  xknot - The knot location
%  sense - The sense (+/-)
%  dim   - For multidimensional regression, this indicates the dimension
%          that this basis function represents.
%  parent- The parent basis function (allows nonlinear interaction among
%          basis functions for multidimensional cases).  For each child
%          BasisFunction, the dimension that it acts on (dim) should be
%          different from all of its parents.  This can be checked by
%          calling represented_dims to determine how many dims are
%          currently represented by the BasisFunction.
%
% Example 1:
%  b = BasisFunction( 2, 0.5, 1 );
%  x=linspace(0,1)';
%  plot(x,b(x));
%
% Example 2:
%  xknot = rand(1,3);
%  sense = ones(1,3);
%  b = BasisFunction(3,xknot,sense);
%  x = [ linspace(0,1)', linspace(-1,1)', linspace(1,2)' ];
%  plot(x,b(x));
%

% AUTHOR: James C. Sutherland
% DATE: December, 2008

switch nargin

  case 0  % default constructor
    B.order      = 0;
    B.xknot      = 0;
    B.sense      = 1;
    B.dim        = 0;
    B.haveParent = 0;
    B.parent     = [];
  
  case 1  % copy constructor
    b = varargin{1};
    if( isa(b,'Basisfunction') )
      B.order      = b.order;
      B.xknot      = b.xknot;
      B.sense      = b.sense;
      B.dim        = b.dim;
      B.haveParent = b.haveParent;
      B.parent     = b.parent;
    else
      msg = strcat('Cannot copy construct a BasisFunction object from an object of type ',class(b));
      error(msg);
    end
    
  case {3,4,5}
    
    iarg = 1;
        
    B.order = varargin{iarg};  iarg=iarg+1;
    xk = varargin{iarg};       iarg=iarg+1;
    [nr,nc] = size(xk);
    if nc>1 || nr>1
      error('knot location must be a single number');
    elseif nr>1
      xk = xk';
    end
    B.xknot = xk;
    s = varargin{iarg};  iarg=iarg+1;
    [nr,nc]=size(s);
    if nc>1 || nr>1
      error('sense vector must be a single number');
    end
    B.sense = s;

    if nargin==3
      B.dim = 1;
      B.haveParent = 0;
      B.parent     = [];
    else
      assert( isnumeric(varargin{iarg}) );
      B.dim = varargin{iarg};  iarg=iarg+1;
      if nargin==5
        assert( isa(varargin{iarg},'BasisFunction') || isa(varargin{iarg},'UnitBasis') );
        B.haveParent = 1;
        B.parent = varargin{iarg};
        iarg=iarg+1;
      else
        B.haveParent = 0;
        B.parent = [];
      end
    end

  otherwise
  error('Invalid use of BasisFunction constructor');
  
end

B = class(B,'BasisFunction');

% public methods
methods (Access=public)
  print_basis(B,fileID);
end % public methods

% error checking
if( any( abs(B.sense)-1 ) )
  error('invalid value for sense.  Must be +/- 1');
end
if( any(B.order < 0) )
  error('invalid order');
end
assert( length(B.order)==1 );
assert( all(size(B.xknot) == size(B.sense)) );
