function order = represented_orders( b )
% interrogate this BasisFunction object to determine how many dimensions
% are represented by it.

if b.haveParent
  order = [ b.order, represented_orders(b.parent) ];
else
  order = b.order;
end