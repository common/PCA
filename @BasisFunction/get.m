function val = get( B, propname )
% val = get( B, propname )

% AUTHOR: James C. Sutherland
% DATE: December, 2008

switch lower(propname)
  case {'order'}
    val = B.order;
  case {'xknot' 'knot' 'x'}
    val = B.xknot;
  case {'sense' 'sign'}
    val = B.sense;
  case 'parent'
    val = B.parent;
  case {'dimension','dim'}
    val = B.dim;
  otherwise
    error( strcat('uknown property: ',propname) );
end