% This Script is a procedure to obtain the R-squared values of 
% reconstructing the Source terms of the PCs using PCA or PCA/MARS

% At first you should do the following lines:
% load bigdata file using ODTdriver.m (you should include the last species as well)
% run h-Zi driver
% load bigsource file

% remove the stuffs related to h-Zi driver
% remove the last species from bigdata & bigsource files
clear Nelem a big_h_Zi filepath
bigdata=bigdata(:,1:11);
bigsource=bigsource(:,1:11);


% Now for PCA/MARS we should follow these lines of codes:

pca=PCA(bigdata,3,'vast');
eta=x2eta(pca,bigdata);
Seta=x2eta(pca,bigsource);

mg3=MarsGroup(eta,Seta);
mg2=MarsGroup(eta(:,1:2),Seta(:,1:2));

x = linspace(0,domainlength,npts);
unit=x(2);

% setting the filter width (e.g. 1,4,8,16 or 32 in paper)
delta =32;
%==========================================================================
% use etabar driver
totallines = nfiles*length(odttimes);
etabar = [];
for i=1:totallines
   n = 898*(i-1);
   etabar = [etabar; ODTfilter(x,eta(n+1:n+898,:),delta*unit) ];
end
clear i n totallines
%==========================================================================

%==========================================================================
% use Setabar driver
totallines = nfiles*length(odttimes);
Setabar = [];
for i=1:totallines
   n = 898*(i-1);
   Setabar = [Setabar; ODTfilter(x,Seta(n+1:n+898,:),delta*unit) ];
end
clear i n totallines
%==========================================================================

% Calculating R-squared values for retaining 3 PCs 
nSeta = mg3(etabar);
r2_3 = zeros(1,3);
for i=1:3
r2_3(i) = 1 - sum((Setabar(:,i)-nSeta(:,i)).^2) / sum((Setabar(:,i)-mean(Setabar(:,i))).^2);
end

clear nSeta

% Calculating R-squared values for retaining 2 PCs 
nSeta=mg2(etabar(:,1:2));
r2_2 = zeros(1,2);
for i=1:2
r2_2(i) = 1 - sum((Setabar(:,i)-nSeta(:,i)).^2) / sum((Setabar(:,i)-mean(Setabar(:,i))).^2);
end

clear nSeta etabar r2_2 r2_3 
clear Setabar


%==========================================================================
% plot maker (which produces th plot of figure #9 in the paper.

delta = [1,4,8,16,32];
vars = [0.907389510199922, 0.916824267612843, 0.921265916428271, 0.923772769164458, 0.920552969153128
        0.937136784076390, 0.947483555393797, 0.951121212275818, 0.949215006202476, 0.938166746443702];
subplot(121);
plot(delta,vars,'.-');
xlabel('\Delta/\Delta_x');ylabel('R^2');title('S_{\eta_1}');
legend('MARS0 n_{\eta}=2','MARS0 n_{\eta}=3');
clear vars 
axis tight

vars = [0.877629388145636, 0.903035377207651, 0.914025236582936, 0.920838975083650, 0.920136763857696
        0.897523538835923, 0.926018815868848, 0.938350110461658, 0.945185681293758, 0.940068085546628];
subplot(122);
plot(delta,vars,'.-');
xlabel('\Delta/\Delta_x');ylabel('R^2');title('S_{\eta_2}');
legend('MARS0 n_{\eta}=2','MARS0 n_{\eta}=3');
clear vars 
axis tight