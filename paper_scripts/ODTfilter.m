function ybar = ODTfilter( x, y, Delta )
% produces a smooth plot for the variable at the ODT line using the
% XXX function for filtering. 
%
% ybar = filtering( y, x, Delta )
%
% INPUTS:
%   y     - dependent variables matrix, each column presents a property and
%           each row presents a point in a grid
%   x     - a vector containing the grid points positions on the ODT line
%   Delta - the filtering function width - default value is 0.0001
%
% Examples:
%   filtering(x, y, 0.0001);
%   filtering(x, y);
%

switch nargin
   case 3
   case 2
      Delta = 0.0001;
   otherwise
      error('improper use of GODTDataFile::filtering');
end

pn = length(x);
for i=1:pn
   t = y(i,:);
   up = true;
   down = true;
   d = 1;
   no = 1;
   while (up==true) || (down==true)
      if up==true
         if (i+d)<=pn && abs(x(i+d)-x(i))<=Delta
            t(1,:)=t(1,:)+y(i+d,:);
            no=no+1;
         else
            up=false;
         end
      end
      if down==true
         if (i-d)>0 && abs(x(i-d)-x(i))<=Delta
            t(1,:)=t(1,:)+y(i-d,:);
            no=no+1;
         else
            down=false;
         end
      end
      d=d+1;
   end
   ybar(i,:)=t(1,:)/no;
end

% figure;
%    plot(x,ybar);


