
% !!! NOTE: you may want to change the path to the ODT data files !!!

clear; clc; close all;

odttimes = [1,18:26,2:17];
npts = 898;
domainlength = 0.013425;

varvec = [21,39:40,42:49];   % state variables without N2. N2 is the 41st 
                             % member, in the cases that we need it we have
                             % to include it in varvec
                             
% the alternative form for varvec is this when we want to extract mixtur-
% fraction, Dissipation rate etc. for flamelet model

% varvec = [24,38,21,39:40,42:49,41];

%%%%%%%%%%%%%%%%%%%%%%%%%%  C constant   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bigdata=[];
nfiles = 30;   % number of realizations
for i=1:nfiles
   filepath    = ['/Users/Amir/AmirWorks/projects/PCATemporalJet/ODT_data/Run',int2str(i),'/fields.h5'];
   fileinfo    = hdf5info(filepath);
   realization = fileinfo.GroupHierarchy;
   for j=1:length(odttimes)
      varinfo=[];
      for k=1:length(varvec)
         varinfo = [varinfo, hdf5read(realization.Groups(odttimes(j)).Datasets(varvec(k)))];
      end
      bigdata = [bigdata; varinfo];
   end
end

clear  i j k realization varinfo filepath 

%% databar driver
% This is to filter the original data which exists in bigdata matrix.
totallines = nfiles*length(odttimes);
x = linspace(0,domainlength,npts);
unit=x(2);
bigdatabar = [];
for i=1:totallines
   n = 898*(i-1);
   bigdatabar = [bigdatabar; ODTfilter(x,bigdata(n+1:n+898,:),32*unit) ];
end
clear doaminlength i n totallines

%% etabar driver
% This is to filter the PCs which exist in eta matrix.
totallines = nfiles*length(odttimes);
etabar = [];
for i=1:totallines
   n = 898*(i-1);
   etabar = [etabar; ODTfilter(x,eta(n+1:n+898,:),8*unit) ];
end
clear i n totallines

%% Setabar driver
% This is to filter the source terms of the PCs which exist in Seta matrix.
totallines = nfiles*length(odttimes);
Setabar = [];
for i=1:totallines
   n = 898*(i-1);
   Setabar = [Setabar; ODTfilter(x,Seta(n+1:n+898,:),0.001) ];
end
clear i n totallines

%% hetabar driver
% This is to filter heta which exists in heta matrix.
totallines = nfiles*length(odttimes);
hetabar = [];
for i=1:totallines
   n = 898*(i-1);
   hetabar = [hetabar; ODTfilter(x,heta(n+1:n+898,:),0.0005) ];
end
clear i n totallines
%% h-Zi driver
% This should be used priviously to calculate the Source terms in Source
% terms driver
hvec = [27,25,22,52,53];
bigh=[];
for i=1:nfiles
   filepath    = ['/Users/Amir/AmirWorks/projects/PCATemporalJet/ODT_data/Run',int2str(i),'/fields.h5'];
   fileinfo    = hdf5info(filepath);
   realization = fileinfo.GroupHierarchy;
   for j=1:length(odttimes)
      h_info=[];
      for k=1:length(hvec)
         h_info = [h_info, hdf5read(realization.Groups(odttimes(j)).Datasets(hvec(k)))];
      end
      bigh = [bigh; (h_info(:,1)- 0.5*h_info(:,3).*(h_info(:,4).^2+h_info(:,5).^2) +h_info(:,2))./h_info(:,3)];
   end
end

a = [ 2 0 0 1 2 1 1 0 0 1 0
      0 2 1 1 1 0 2 1 2 1 0
      0 0 0 0 0 0 0 1 1 1 0
      0 0 0 0 0 0 0 0 0 0 2];
Nelem = 4;
wl = [1 16 12 14];
wi = [2 32 16 17 18 1 33 28 44 29 28];
big_h_Zi = zeros(size(bigdata,1),Nelem);
for i =1:Nelem
   for j=1:length(varvec)-1
      big_h_Zi(:,i) = big_h_Zi(:,i)+ (a(i,j)*wl(i)/wi(j)) * bigdata(:,j+1);
   end
end

big_h_Zi = [bigh,big_h_Zi];

clear i j k realization h_info h_vec bigh fileinfo wi wl hvec

%% Source terms driver
% This block calculates the Source terms of the original data.
varvec = 11:20;
STinfovec = [1:10,22,23];

STinfo=[];
bigsource=[];
for i=1:nfiles
   filepath    = ['/Users/Amir/AmirWorks/projects/PCATemporalJet/ODT_data/Run',int2str(i),'/fields.h5'];
   fileinfo    = hdf5info(filepath);
   realization = fileinfo.GroupHierarchy;
   for j=1:length(odttimes)
      ST=[];
      varinfo=[];
      for k=1:length(STinfovec)
         ST = [ST, hdf5read(realization.Groups(odttimes(j)).Datasets(STinfovec(k)))];
         if k<=length (varvec)
            varinfo=[varinfo, hdf5read(realization.Groups(odttimes(j)).Datasets(varvec(k)))];
         end
      end
      STinfo = [STinfo; ST];
      bigsource=[bigsource;varinfo];
   end
end
for i=1:length(varvec)
   bigsource(:,i)=bigsource(:,i)./STinfo(:,11);
end
hn = (big_h_Zi(:,1)-sum(STinfo(:,1:10).*bigdata(:,2:11),2))./bigdata(:,12);

STinfo=[STinfo(:,1:10),hn,STinfo(:,11:12)];
bigsource=[bigsource, -sum(bigsource,2)];
Tsource = sum(STinfo(:,1:11).*bigsource,2)./(STinfo(:,12).*STinfo(:,13));
bigsource= [Tsource,bigsource];

clear varvec STinfovec STinfo fileinfo realization i k j ST varinfo Tsource


%% comparing 1st four eig-vectors of VAST and PARETO scaling on bigdatabar

% pca2 = PCA(bigdata,3,'VAST');
% pca3 = PCA(bigdata,3,'PARETO');
Y=[abs(pca2.Q(:,1)),abs(pca3.Q(:,1)),abs(pca2.Q(:,2)),abs(pca3.Q(:,2)),abs(pca2.Q(:,3)),abs(pca3.Q(:,3)),abs(pca2.Q(:,4)),abs(pca3.Q(:,4))];

subplot 221
bar(Y(:,1:2));
legend('STD','PARETO');
set(gca,'XTickLabel',{'T','H2','O2','O','OH','H2O','H','HO2','CO','CO2','HCO','N2'});
title('1st eig-vector weights');

subplot 222
bar(Y(:,3:4));
legend('STD','PARETO');
set(gca,'XTickLabel',{'T','H2','O2','O','OH','H2O','H','HO2','CO','CO2','HCO','N2'});
title('2nd eig-vector weights');

subplot 223
bar(Y(:,5:6));
legend('STD','PARETO');
set(gca,'XTickLabel',{'T','H2','O2','O','OH','H2O','H','HO2','CO','CO2','HCO','N2'});
title('3rd eig-vector weights');

subplot 224
bar(Y(:,7:8));
legend('STD','PARETO');
set(gca,'XTickLabel',{'T','H2','O2','O','OH','H2O','H','HO2','CO','CO2','HCO','N2'});
title('4th eig-vector weights');