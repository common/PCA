% This Script is a procedure to obtain the R-squared values of 
% reconstructing the original data using SLFM or SLFM-beta_pdf

% For SLFM we should follow these codes:

% Atfirst we need to obtain mixture fraction and scalar dissipation rate
% from the data set
ind = bigdata(:,1)>1;
bigdata(ind,1) = 1;
clear ind
ind = bigdata(:,1)<0;
bigdata(ind,1) = 0;
clear ind

bigdata(:,2) = bigdata(:,2)./exp((-2)*(erfinv(2*bigdata(:,1)-1)).^2);

% setting the filter width (e.g. 1,4,8,16 or 32 in paper)
delta =32;
%==========================================================================
% use databar driver here
totallines = nfiles*length(odttimes);
x = linspace(0,domainlength,npts);
bigdatabar = [];
for i=1:totallines
   n = 898*(i-1);
   bigdatabar = [bigdatabar; ODTfilter(x,bigdata(n+1:n+898,:),delta*unit) ];
end
clear doaminlength i n totallines
%==========================================================================
ind = bigdatabar(:,2)>2189;
bigdatabar(ind,2) = 2200;
clear ind
ind = bigdatabar(:,2)<0.01;
bigdatabar(ind,2) = 0.01;
clear ind
ind = bigdatabar(:,1)>1;
bigdatabar(ind,1) = 1;
clear ind
ind = bigdatabar(:,1)<0;
bigdatabar(ind,1) = 0;
clear ind

% Now we are ready to regress and recalculate the data set
rec_databar = [];                  %Temperature
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,3));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %H2
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,4));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %O2
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,5));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %O
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,6));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %OH
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,7));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %H2O
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,8));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %H
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,9));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %HO2
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,10));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %CO
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,11));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %CO2
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,12));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %HCO
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,13));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];          %N2
m = mars ([bigdatabar(:,1),bigdatabar(:,2)],bigdatabar(:,14));
temp = m(bigdatabar(:,1),bigdatabar(:,2));
rec_databar=[rec_databar,temp];

% Calculating R-squared values for SLFM
nvar=12;
r2 = zeros(1,nvar);
for i=1:nvar
r2(i) = 1 - sum((bigdatabar(:,i+2)-rec_databar(:,i)).^2) / sum((bigdatabar(:,i+2)-mean(bigdatabar(:,i+2))).^2);
end

clear temp tempM nvar i fin r2
clear rec_databar bigdatabar
npts=898;


clear Dis F dvar dvarNames

%% 
% For SLFM-beta_pdf we should follow these codes:

% Atfirst we need to obtain mixture fraction and scalar dissipation rate
% and variance of the mixture fraction from the data set
ind = bigdata(:,1)>1;
bigdata(ind,1) = 1;
clear ind
ind = bigdata(:,1)<0;
bigdata(ind,1) = 0;
clear ind

ind1 = bigdata(:,1)<0.9;
ind2 = bigdata(:,1)>0.1;
ind = (ind1==ind2);
bigdata(ind,2) = bigdata(ind,2)./exp((-2)*(erfinv(2*bigdata(ind,1)-1)).^2);
clear ind1 ind2 ind

f2=bigdata(:,1).^2;
bigdata=[bigdata(:,1),f2,bigdata(:,2:14)];
clear f2

% setting the filter width (e.g. 1,4,8,16 or 32 in paper)
delta =32;
%==========================================================================
% use databar driver here
totallines = nfiles*length(odttimes);
x = linspace(0,domainlength,npts);
bigdatabar = [];
for i=1:totallines
   n = 898*(i-1);
   bigdatabar = [bigdatabar; ODTfilter(x,bigdata(n+1:n+898,:),delta*unit) ];
end
clear doaminlength i n totallines
%==========================================================================

ind = bigdatabar(:,3)>2189;
bigdatabar(ind,3)=2200;
clear ind
ind = bigdatabar(:,3)<0.01;
bigdatabar(ind,3) = 0.01;
clear ind
ind = bigdatabar(:,1)>1;
bigdatabar(ind,1) = 1;
clear ind
ind = bigdatabar(:,1)<0;
bigdatabar(ind,1) = 0;
clear ind
ind = bigdatabar(:,2)>1;
bigdatabar(ind,2) = 1;
clear ind
ind = bigdatabar(:,2)<0;
bigdatabar(ind,2) = 0;
clear ind

var=zeros(length(bigdatabar),1);
ind1 = bigdatabar(:,1)~=1;
ind2 = bigdatabar(:,1)~=0;
ind = (ind1==ind2);
var(ind,1) = (bigdatabar(ind,2)-bigdatabar(ind,1).^2)./(bigdatabar(ind,1).*(1-bigdatabar(ind,1)));
clear ind1 ind2 ind

% Now we are ready to regress and recalculate the data set
rec_databar=[];                          %Temperature
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,4));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %H2
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,5));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %O2
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,6));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %O
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,7));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %OH
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,8));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %H2O
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,9));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %H
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,10));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %HO2
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,11));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %CO
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,12));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %CO2
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,13));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %HCO
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,14));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];          %N2
m = mars ([bigdatabar(:,1),bigdatabar(:,3),var],bigdatabar(:,15));
temp = m([bigdatabar(:,1),bigdatabar(:,3),var]);
rec_databar=[rec_databar,temp];

% Calculating R-squared values for SLFM
nvar=12;
r2 = zeros(1,nvar);
for i=1:nvar
r2(i) = 1 - sum((bigdatabar(:,i+3)-rec_databar(:,i)).^2) / sum((bigdatabar(:,i+3)-mean(bigdatabar(:,i+3))).^2);
end

clear temp tempM nvar i fin r2
clear rec_databar bigdatabar
npts=898;
