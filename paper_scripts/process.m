% This Script is a procedure to obtain the R-squared values of 
% reconstructing the original data using PCA or PCA/MARS

% At first you should load bigdata file using ODTdriver.m

% For PCA we should follow these codes:

% !!!If these are not specified like the following lines we need to 
% uncomment these lines here!!!

% x=linspace(0,0.013425,898);
% unit=x(2)-x(1)-0.0001e-5
% nvar=11;

% setting the filter width (e.g. 1,4,8,16 or 32 in paper)
delta =32;
%==========================================================================
% use databar driver here
totallines = nfiles*length(odttimes);
x = linspace(0,domainlength,npts);
bigdatabar = [];
for i=1:totallines
   n = 898*(i-1);
   bigdatabar = [bigdatabar; ODTfilter(x,bigdata(n+1:n+898,:),delta*unit) ];
end
clear doaminlength i n totallines
%==========================================================================

pca=PCA(bigdata,3,'vast');
eta=x2eta(pca,bigdata);

%==========================================================================
% use etabar driver
totallines = nfiles*length(odttimes);
etabar = [];
for i=1:totallines
   n = 898*(i-1);
   etabar = [etabar; ODTfilter(x,eta(n+1:n+898,:),delta*unit) ];
end
clear i n totallines
%==========================================================================

% Calculating R-squared values for retaining 3 PCs 
nbigdata=eta2x(pca,etabar);
r2_3 = zeros(1,nvar);
for i=1:nvar
r2_3(i) = 1 - sum((bigdatabar(:,i)-nbigdata(:,i)).^2) / sum((bigdatabar(:,i)-mean(bigdatabar(:,i))).^2);
end

clear nbigdata 

% Calculating R-squared values for retaining 2 PCs 
pca.neta=2;
nbigdata=eta2x(pca,etabar(:,1:2));
r2_2 = zeros(1,nvar);
for i=1:nvar
r2_2(i) = 1 - sum((bigdatabar(:,i)-nbigdata(:,i)).^2) / sum((bigdatabar(:,i)-mean(bigdatabar(:,i))).^2);
end

clear pca eta nbigdata etabar r2_2 r2_3
clear bigdatabar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For PCA/MARS we should follow these codes:

pca=PCA(bigdata,3,'vast');
eta=x2eta(pca,bigdata);
vn=[1,2,5,6,9,10];
mg3=MarsGroup(eta,bigdata(:,vn),1e-12,2,20,150);
mg2=MarsGroup(eta(:,1:2),bigdata(:,vn),1e-12,2,20,150);

% setting the filter width (e.g. 1,4,8,16 or 32 in paper)
delta=32;
%==========================================================================
% use databar driver here
totallines = nfiles*length(odttimes);
x = linspace(0,domainlength,npts);
bigdatabar = [];
for i=1:totallines
   n = 898*(i-1);
   bigdatabar = [bigdatabar; ODTfilter(x,bigdata(n+1:n+898,:),delta*unit) ];
end
clear doaminlength i n totallines
%==========================================================================

%==========================================================================
% use etabar driver
totallines = nfiles*length(odttimes);
etabar = [];
for i=1:totallines
   n = 898*(i-1);
   etabar = [etabar; ODTfilter(x,eta(n+1:n+898,:),delta*unit) ];
end
clear i n totallines
%==========================================================================

% Calculating R-squared values for retaining 3 PCs 
nbigdata=mg3(etabar);
r2_3 = zeros(1,6);
for i=1:6
r2_3(i) = 1 - sum((bigdatabar(:,vn(i))-nbigdata(:,i)).^2) / sum((bigdatabar(:,vn(i))-mean(bigdatabar(:,vn(i)))).^2);
end

clear nbigdata 


% Calculating R-squared values for retaining 2 PCs 
nbigdata=mg2(etabar(:,1:2));
r2_2 = zeros(1,6);
for i=1:6
r2_2(i) = 1 - sum((bigdatabar(:,vn(i))-nbigdata(:,i)).^2) / sum((bigdatabar(:,vn(i))-mean(bigdatabar(:,vn(i)))).^2);
end

clear nbigdata etabar r2_2 r2_3 
clear bigdatabar



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use databar driver here

bpca=PCA(bigdatabar,3,'vast');
bpca.Q