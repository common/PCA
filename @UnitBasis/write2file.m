function write2file( B, fid)
%Writes the UnitBasis to a .txt file
%
% Example:
%  UB = UnitBasis();
%  fid = fopen('test.txt','w');
%  wite2file(UB,fid);
%
% INPUTS:
%  B   - the UnitBasis function
%  fid - destination data file ID in text format that we want to write in 
%        it and it is created by fopen
% 
% NOTE: 
% 
%
% See also 

% AUTHOR: Amir Biglari
% DATE: August, 2013
fprintf(fid, '%s\n','UnitBasis');

end

