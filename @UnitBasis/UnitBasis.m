classdef UnitBasis
  
  methods (Access=public)
    
    function UB = UnitBasis()
    end
    
    function y=subsref(B,S)
      switch S.type
        case '()'
          x = S.subs{:};
          [npts,nvar] = size(x);
          y = ones(npts,1);
        otherwise
          error('invalid use of subsref');
      end
    end
    
    function orders = represented_orders(ub)
      orders = [];
    end
    
    function dims = represented_dims(ub)
      dims = [];
    end
    
    function result = eq(B1,B2)
      if isa(B2,'UnitBasis')
        result = 1;
      else
        result = 0;
      end
    end
    
    function display(ub)
      fprintf('Unit basis function\n');
    end
    
  end % public methods
  
end % classdef UnitBasis